package com.md.spring.core;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;

public class Triangle {

	private List<Point> points;

	private HashMap<Point, String> pointMap;

	public void drawTriangle() {
		for (Point p : points) {
			System.out.println("Poing : (" + p.getX() + "," + p.getY() + ")");
		}
	}

	public void showMap() {
		for (Entry<Point, String> entry : pointMap.entrySet()) {
			System.out.println("(" + entry.getKey().getX() + "," + entry.getKey().getY() + ") :" + entry.getValue());
		}
	}

	public List<Point> getPoints() {
		return points;
	}

	public void setPoints(List<Point> points) {
		this.points = points;
	}

	public HashMap<Point, String> getPointMap() {
		return pointMap;
	}

	public void setPointMap(LinkedHashMap<Point, String> pointMap) {
		this.pointMap = pointMap;
	}

}
