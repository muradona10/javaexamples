package com.md.spring.core;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class DrawApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		ApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
		Triangle triangle = (Triangle) context.getBean("triangle");
		triangle.drawTriangle();
		triangle.showMap();
		
		/**
		 * spring.xml dosyas� i�erisinde Triangle objesinin tan�m� bulunuyor
		 * Buna g�re Triangle point nesnelerinden olu�an bir liste tutuyor
		 * Tan�m�na g�re bu liste bir property ve bu property i�inde 3 elemandan olu�an bir liste var
		 * Benzer �ekilde SET ve MAP te yap�labilir.
		 */
	}

}
