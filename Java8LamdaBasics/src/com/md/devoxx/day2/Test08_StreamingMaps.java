package com.md.devoxx.day2;

import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


public class Test08_StreamingMaps {
	
	private List<String> sonnet = List.of(
            "From fairest creatures we desire increase,",
            "That thereby beauty's rose might never die,",
            "But as the riper should by time decease,",
            "His tender heir might bear his memory:",
            "But thou contracted to thine own bright eyes,",
            "Feed'st thy light's flame with self-substantial fuel,",
            "Making a famine where abundance lies,",
            "Thy self thy foe, to thy sweet self too cruel:",
            "Thou that art now the world's fresh ornament,",
            "And only herald to the gaudy spring,",
            "Within thine own bud buriest thy content,",
            "And, tender churl, mak'st waste in niggarding:",
            "Pity the world, or else this glutton be,",
            "To eat the world's due, by the grave and thee.",
            "to to thy");
	
	private List<String> alphabet =
			List.of("alfa", "bravo", "charlie", "delta", "echo",
                    "foxtrot", "golf", "hotel", "india", "juliet",
                    "kilo", "lima", "mike", "november", "oscar",
                    "papa", "quebec", "romeo", "sierra", "tango",
                    "uniform", "victor", "whiskey", "x-ray", "yankee",
                    "zulu");

    /**
     * Find the most frequently occurring words in the Sonnet
     */
    public void streamingMaps_1() {

        Pattern pattern = Pattern.compile(("[ ,':\\-]+"));

        /**
         * All words in sonnet and their frequencies
         */
        Map<String, Long> words =
                sonnet.stream()
                        .map(String::toLowerCase)
                        .flatMap(pattern::splitAsStream) // line -> pattern.splitAsStream(line)
                        .collect(
                        		 Collectors.collectingAndThen(
                        				 Collectors.groupingBy(
                        						 letter -> letter,
                        						 Collectors.counting()
                        						 ), map -> map
                        			)
                        		);
                      

        words.forEach((letter, count) -> System.out.println(letter + " => " + count));
        System.out.println("-------------------------------------------------------------------------");

        /**
         * max(comparator) -> iterate in all words and find the maximum map value and return it with key and value pair
         * Map.Entry.comparingByValue() is a comparator for a map, compares map values 
         */
        Map.Entry<String, Long> mostFrequentWord =
                words.entrySet().stream() // Stream<Map.Entry<String, Long>>
                        .max(Map.Entry.comparingByValue()).get();
        System.out.println("mostFrequentWord = " + mostFrequentWord);
        System.out.println("-------------------------------------------------------------------------");

        /**
         * iterate in all words and create a new Map<Long, List<String>> by grouping them with word 
         * frequency is key and list of string has same frequency
         */
        Map<Long, List<String>> otherWords =
                words.entrySet().stream() // Stream<Map.Entry<String, Long>>
                        .collect(
                                Collectors.groupingBy(
                                        Map.Entry::getValue,   // x -> x.getValue(),
                                        Collectors.mapping(
                                                Map.Entry::getKey,
                                                Collectors.toList()
                                        )
                                )
                        );
        otherWords.forEach((letter, count) -> System.out.println(letter + " => " + count));
        System.out.println("-------------------------------------------------------------------------");

        Map.Entry<Long, List<String>> mostSeenWords =
                otherWords.entrySet().stream() // Stream<Map.Entry<Long, List<String>>>
                        .max(Map.Entry.comparingByKey()).get();
        System.out.println("mostSeenWords = " + mostSeenWords);
        System.out.println("-------------------------------------------------------------------------");
    }
    
    public List<List<String>> streamingIndex_01(int N) {
    	int SIZE = alphabet.size();
    	return IntStream.range(0, SIZE-N+1)                    // rangeClosed is efficient
    			.mapToObj( o -> alphabet.subList(o, o+N))
    			.collect(Collectors.toList());
    }
    
    public List<List<String>> streamingIndex_02() {
    	int SIZE = alphabet.size();
    	
    	/**
    	 * finds next word size greater than first one index
    	 */
    	List<Integer> breaks =  IntStream.range(1, SIZE)
    									 .filter(m -> alphabet.get(m).length() < alphabet.get(m-1).length())
    									 .boxed()
    									 .collect(Collectors.toList());
    	breaks.add(0, 0);
    	breaks.add(alphabet.size());
    	return IntStream.range(0, breaks.size()-1)
    			        .mapToObj(i->alphabet.subList(breaks.get(i), breaks.get(i+1)))
    			        .collect(Collectors.toList());
    	
    }
    

	public static void main(String[] args) {
		Test08_StreamingMaps test = new Test08_StreamingMaps();
		//test.streamingMaps_1();
		System.out.println("-------------------------------------------------------------------------");
		System.out.println("-------------------------------------------------------------------------");
		//test.streamingIndex_01(3).forEach(i -> System.out.println(i));
		System.out.println("-------------------------------------------------------------------------");
		test.streamingIndex_02().forEach(i -> System.out.println(i));
	}

}
