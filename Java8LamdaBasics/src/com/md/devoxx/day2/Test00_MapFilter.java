package com.md.devoxx.day2;

import java.util.List;

public class Test00_MapFilter {
	
	private List<String> alphabet = List.of("alfa", "bravo", "charlie", "delta", "echo",
                    "foxtrot", "golf", "hotel", "india", "juliet",
                    "kilo", "lima", "mike", "november", "oscar",
                    "papa", "quebec", "romeo", "sierra", "tango",
                    "uniform", "victor", "whiskey", "x-ray", "yankee",
                    "zulu");
	
	/**
	 * map -> uppercase all strings
	 * filter -> only take strings that lenght is equal to 6
	 * write all filtered strings
	 */
    public void mapFilter_1() {
        alphabet.stream()
                .map(String::toUpperCase) // method reference, lambda version is "s -> s.toUpperCase()"
                .filter(w -> w.length() == 6) // lambda expression
                .forEach(System.out::println); // method reference, lambda version is "s -> System.out.println(s)"
    }
    
	public static void main(String [] args) {
		Test00_MapFilter test = new Test00_MapFilter();
		test.mapFilter_1();
	}

}
