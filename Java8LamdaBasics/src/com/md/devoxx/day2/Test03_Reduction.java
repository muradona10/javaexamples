package com.md.devoxx.day2;

import java.math.BigInteger;
import java.util.stream.LongStream;

public class Test03_Reduction {

	public static void main(String [] args) {
		Test03_Reduction test = new Test03_Reduction();
		System.out.println("21! is " + test.mapFilter_2(21));
		System.out.println("60! is " + test.mapFilter_2(60));
	}
	
    /**
     * n!, factorial operation
     * Reduction is here, first parameter is start point, and second parameter is the function we want
     * @return
     */
    public BigInteger mapFilter_2(int number) {
        BigInteger result = LongStream.rangeClosed(1, number)
                .mapToObj(BigInteger::valueOf) // method reference, lambda version is "m -> BigInteger.valueOf(m)"
                .reduce(BigInteger.ONE, BigInteger::multiply); // method reference "(m,n) -> m.multiply(n)"
        return result;
    }
	
}
