package com.md.devoxx.day2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class Test06_GroupingBy {
	
	private List<String> alphabet =
			List.of("alfa", "bravo", "charlie", "delta", "echo",
                    "foxtrot", "golf", "hotel", "india", "juliet",
                    "kilo", "lima", "mike", "november", "oscar",
                    "papa", "quebec", "romeo", "sierra", "tango",
                    "uniform", "victor", "whiskey", "x-ray", "yankee",
                    "zulu");

    private List<String> sonnet = List.of(
            "From fairest creatures we desire increase,",
            "That thereby beauty's rose might never die,",
            "But as the riper should by time decease,",
            "His tender heir might bear his memory:",
            "But thou contracted to thine own bright eyes,",
            "Feed'st thy light's flame with self-substantial fuel,",
            "Making a famine where abundance lies,",
            "Thy self thy foe, to thy sweet self too cruel:",
            "Thou that art now the world's fresh ornament,",
            "And only herald to the gaudy spring,",
            "Within thine own bud buriest thy content,",
            "And, tender churl, mak'st waste in niggarding:",
            "Pity the world, or else this glutton be,",
            "To eat the world's due, by the grave and thee.");


    
    /**
     * 
     * @return 
     */
    public static Map<Integer, List<String>> groupingBy_01(List<String> listOfString) {
    	return listOfString.stream()
    			           .collect(Collectors.toMap(
    			        		   String::length,                         // KEY, "m -> m.toString().length()"
                                   s -> new ArrayList<>(Arrays.asList(s)), // VALUE
                                   (a, b) -> {                             // MERGE FUNCTION
                                       a.addAll(b);
                                       return a;
                                   }));
    }
    
    public static Map<Integer, List<String>> groupingBy_02(List<String> listOfString) {
    	return listOfString.stream()
    			           .collect(Collectors.groupingBy(String::length));
    }
    
    public static Map<String, List<String>> groupingBy_03(List<String> listOfString) {
    	return listOfString.stream()
    			           .collect(Collectors.groupingBy( x -> x.substring(0,1)));
    }

    
    
    public static <T,U> void printMap(Map<T, U> map) {
    	for(T item : map.keySet()) {
    		System.out.println(item + ": " + map.get(item));
    	}
    }
    
    public static void main(String[] args) {
        Test06_GroupingBy test = new Test06_GroupingBy();
        printMap(groupingBy_01(test.alphabet));
    	System.out.println("-----------------------------------------------------------");
    	printMap(groupingBy_02(test.alphabet));
    	System.out.println("-----------------------------------------------------------");
    	printMap(groupingBy_03(test.sonnet));
    	System.out.println("-----------------------------------------------------------");
    
    	
    }

}
