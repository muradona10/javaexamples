package com.md.devoxx.day2;

import java.util.List;
import java.util.function.IntUnaryOperator;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Test04_FunctionCombination {

	public static void main(String [] args) {
		List<String> strings = List.of("Red","Purple","Black","Blue","Yellow","Green","Pink");
		strings.stream().filter(functionCombination_1()).collect(Collectors.toList()).forEach(System.out::println);
		System.out.println("---------------------------------------------------------------------");
		
		Integer five = 5;
		System.out.println(functionCombination_2().applyAsInt(five));
		
		
	}
	
	public static Predicate<String> functionCombination_1() {

        List<Predicate<String>> predicates =
        		List.of(s -> s != null, 
        		        s -> !s.isEmpty(), 
        		        s -> s.length() < 6, 
        		        s -> s.substring(0, 1).equals("B"));

        Predicate<String> combinedPredicate =
                predicates.stream().reduce(s -> true, Predicate::and); // "(p,p1) -> p.and(p1)"
        
        return combinedPredicate;

    }

    public static IntUnaryOperator functionCombination_2() {

        List<IntUnaryOperator> operators =
        		List.of(i -> i + 1, i -> i * 2, i -> i + 3);

        IntUnaryOperator combinedOperator =
                operators.stream().reduce(IntUnaryOperator.identity(), IntUnaryOperator::andThen); // i -> i , "(m,n) -> m.andThen(n)"
        
        return combinedOperator;

    }
	
}
