package com.md.devoxx.day2;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


public class Test01_FlatMap {
	
    private List<String> alphabet = List.of("alfa", "bravo", "charlie", "delta");

    private List<String> sonnet = List.of(
            "From fairest creatures we desire increase,",
            "That thereby beauty's rose might never die,",
            "But as the riper should by time decease,",
            "His tender heir might bear his memory:",
            "But thou contracted to thine own bright eyes,",
            "Feed'st thy light's flame with self-substantial fuel,",
            "Making a famine where abundance lies,",
            "Thy self thy foe, to thy sweet self too cruel:",
            "Thou that art now the world's fresh ornament,",
            "And only herald to the gaudy spring,",
            "Within thine own bud buriest thy content,",
            "And, tender churl, mak'st waste in niggarding:",
            "Pity the world, or else this glutton be,",
            "To eat the world's due, by the grave and thee.");


    private List<String> expand(String s) {
        return s.codePoints()
                .mapToObj(codePoint -> Character.toString((char) codePoint))
                .collect(Collectors.toList());
    }

    private String[] splitToWords(String line) {
        return line.split(" +");
    }
    
    public List<List<String>> flatMap_1() {
        List<List<String>> result = alphabet.stream()
                .map(s -> expand(s))
                .collect(Collectors.toList());
        return result;
    }
    
    public List<String> flatMap_1_b() {
        List<String> result = alphabet.stream()
                .flatMap(s -> expand(s).stream())
                .collect(Collectors.toList());
        return result;
    }
    
    public void printListOfList(List<List<String>> listoflist) {
    	listoflist.forEach(l -> {
    								l.forEach(x -> System.out.print(x + "\t")); 
    								System.out.println();
    	   						});
    }
    
    public List<String> flatMap_2() {

        Pattern pattern = Pattern.compile(" +");

        List<String> words = sonnet.stream()
                .flatMap(pattern::splitAsStream)
                .collect(Collectors.toList());
        return words;
    }
    
    public void flatMap_2_b() {
    	sonnet.stream()
    	      .flatMap(l -> Arrays.stream(splitToWords(l)))
    	      .collect(Collectors.toList())
    	      .forEach(System.out::println);
    }
    
    public static void main(String[] args) {
        
    	Test01_FlatMap test = new Test01_FlatMap();
    	test.printListOfList(test.flatMap_1());
    	System.out.println("***************************************************************");
    	test.flatMap_1_b().forEach(System.out::print);
    	System.out.println();
    	System.out.println("***************************************************************");
    	test.flatMap_2().forEach(System.out::println);
    	System.out.println("***************************************************************");
    	test.flatMap_2_b();
    }

}
