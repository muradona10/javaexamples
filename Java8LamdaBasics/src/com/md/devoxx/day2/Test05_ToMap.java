package com.md.devoxx.day2;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class Test05_ToMap {
	
    private List<String> alphabet = List.of("alfa", "bravo", "charlie", "delta");

    private List<String> sonnet = List.of(
            "From fairest creatures we desire increase,",
            "That thereby beauty's rose might never die,",
            "But as the riper should by time decease,",
            "His tender heir might bear his memory:",
            "But thou contracted to thine own bright eyes,",
            "Feed'st thy light's flame with self-substantial fuel,",
            "Making a famine where abundance lies,",
            "Thy self thy foe, to thy sweet self too cruel:",
            "Thou that art now the world's fresh ornament,",
            "And only herald to the gaudy spring,",
            "Within thine own bud buriest thy content,",
            "And, tender churl, mak'st waste in niggarding:",
            "Pity the world, or else this glutton be,",
            "To eat the world's due, by the grave and thee.");

    /**
     * key: first character, value: all string
     * @return map
     */
    public static Map<String, String> toMap_01(List<String> listOfString) {
    	return listOfString.stream()
    			           .collect(Collectors.toMap(s -> s.substring(0, 1), s -> s));
    }
    
    /**
     * key: first character, value: all string if any dublicate key occurs then choose first one
     * @return map
     */
    public static Map<String, String> toMap_02(List<String> listOfString) {
    	return listOfString.stream()
    			           .collect(Collectors.toMap(
    			        		   		s -> s.substring(0, 1), // KEY
    			        		   		s -> s,                 // VALUE
    			        		   		(line1,line2) -> line1  // MERGE FUNCTION FOR DUBLICATES
    			        		   ));
    }

    /**
     * key: first character, value: all string if any duplicate key occurs then choose last one
     * @return map
     */
    public static Map<String, String> toMap_03(List<String> listOfString) {
    	return listOfString.stream()
    			           .collect(Collectors.toMap(
    			        		   		s -> s.substring(0, 1), // KEY
    			        		   		s -> s,                 // VALUE
    			        		   		(line1,line2) -> line2  // MERGE FUNCTION FOR DUBLICATES
    			        		   ));
    }
    
    /**
     * key: first character, value: all string if any duplicate key occurs then choose last one
     * @return map
     */
    public static Map<String, String> toMap_04(List<String> listOfString) {
    	return listOfString.stream()
    			           .collect(Collectors.toMap(
    			        		   		s -> s.substring(0, 1),                // KEY
    			        		   		s -> s,                                // VALUE
    			        		   		(line1,line2) -> line1 + " # " + line2  // MERGE FUNCTION FOR DUBLICATES
    			        		   ));
    }
    
    public static <K,V> void printMap(Map<K, V> map) {
    	for(K item : map.keySet()) {
    		System.out.println(item + ": " + map.get(item));
    	}
    }
    
    public static void main(String[] args) {
        Test05_ToMap test = new Test05_ToMap();
        printMap(toMap_01(test.alphabet));
    	System.out.println("-----------------------------------------------------------");
    	//System.out.println(toMap_01(test.sonnet)); // IllegalStateException, Dublicate key for B letter
    	System.out.println("-----------------------------------------------------------");
    	printMap(toMap_02(test.sonnet));
    	System.out.println("-----------------------------------------------------------");
    	printMap(toMap_03(test.sonnet));
    	System.out.println("-----------------------------------------------------------");
    	printMap(toMap_04(test.sonnet));
    	
    }

}
