package com.md.devoxx.day2;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;


public class Test07_CascadingCollectors {
	
	@SuppressWarnings("unused")
	private List<String> alphabet =
			List.of("alfa", "bravo", "charlie", "delta", "echo",
                    "foxtrot", "golf", "hotel", "india", "juliet",
                    "kilo", "lima", "mike", "november", "oscar",
                    "papa", "quebec", "romeo", "sierra", "tango",
                    "uniform", "victor", "whiskey", "x-ray", "yankee",
                    "zulu");

    private List<String> sonnet = List.of(
            "From fairest creatures we desire increase,",
            "That thereby beauty's rose might never die,",
            "But as the riper should by time decease,",
            "His tender heir might bear his memory:",
            "But thou contracted to thine own bright eyes,",
            "Feed'st thy light's flame with self-substantial fuel,",
            "Making a famine where abundance lies,",
            "Thy self thy foe, to thy sweet self too cruel:",
            "Thou that art now the world's fresh ornament,",
            "And only herald to the gaudy spring,",
            "Within thine own bud buriest thy content,",
            "And, tender churl, mak'st waste in niggarding:",
            "Pity the world, or else this glutton be,",
            "To eat the world's due, by the grave and thee.");


    
    private List<String> expand(String s) {
        return s.codePoints()
                .mapToObj(codePoint -> Character.toString((char) codePoint))
                .collect(Collectors.toList());
    }
    
    public static <T,U> void printMap(Map<T, U> map) {
    	map.forEach((k,v) -> System.out.println(k + ": " + v));
    }

    /**
     * Group the lines of the sonnet stream by each sentences first char and count them
     * Collectors.counting()
     * @return map
     */
    public Map<String, Long> cascadingCollectors_01() {

        Map<String, Long> map =
                sonnet.stream()
                        .collect(Collectors.groupingBy(line -> line.substring(0, 1),
                                Collectors.counting()));
        return map;
    }
    
    /**
     * Group the lines of the sonnet stream by each sentences first char
     * and map with their length and convert them to a list
     * Collectors.mapping()
     * @return map
     */
    public Map<String, List<Integer>> cascadingCollectors_02() {

        Map<String, List<Integer>> map =
                sonnet.stream()
                        .collect(Collectors.groupingBy(line -> line.substring(0, 1),
                        		Collectors.mapping(String::length, Collectors.toList())));
        
        return map;
    }
    
    /**
     * Group the lines of the sonnet by first letter, and collect the first word of grouped lines into a set.
     */
    public Map<String, Set<String>> cascadingCollectors_03() {

        Collector<String, ?, Set<String>> mapToFirstWordInASet =
                Collectors.mapping(
                        (String line) -> line.split(" +")[0], // regular expression " +", separate each sentence word by word and get first word of all sentences
                        Collectors.toSet()
                );
        
        Map<String, Set<String>> map =
                sonnet.stream()
                        .collect(
                                Collectors.groupingBy(
                                        line -> line.substring(0, 1),
                                        mapToFirstWordInASet
                                )
                        );

        return map;
    }
    
    /**
     * Convert sonnet to a flatMap and group the whole string by each char. 
     * And convert to a map keys are char and values are frequency of this keys.
     */
    public Map<String, Long> cascadingCollectors_04() {

        Map<String, Long> map =
                sonnet.stream() // stream of the lines of the sonnet
                        .flatMap(line -> expand(line).stream()) // stream of letters
                        .collect(
                                Collectors.groupingBy(
                                        letter -> letter,
                                        Collectors.counting()
                                )
                        );

        return map;
    }
    
    public static void main(String[] args) {
        Test07_CascadingCollectors test = new Test07_CascadingCollectors();
        printMap(test.cascadingCollectors_01());
        System.out.println("-----------------------------------------------------------");
        printMap(test.cascadingCollectors_02());
        System.out.println("-----------------------------------------------------------");
        printMap(test.cascadingCollectors_03());
        System.out.println("-----------------------------------------------------------");
        printMap(test.cascadingCollectors_04());
    }

}
