package com.md.devoxx.day1;

import java.util.Objects;

import com.md.devoxx.day1.util.Predicate;

public class Play02_Predicates {

	public static void main(String[] args) {
		
		Predicate<String> empty =  String::isEmpty; //s -> s.isEmpty();
		Predicate<String> notEmpty = empty.negate();
		
		System.out.println("empty for Devoxx    : " + empty.test("Devoxx"));
		System.out.println("empty for space     : " + empty.test(""));
		System.out.println("notEmpty for Devoxx : " + notEmpty.test("Devoxx"));

		 Predicate<String> nonNull = Objects::nonNull; //m -> m!=null;
		 Predicate<String> nonNullAndEmpty = empty.and(nonNull);
		 System.out.println("nonNullAndEmpty for Devoxx : " + nonNullAndEmpty.test("Devoxx"));
		 System.out.println("nonNullAndEmpty for space  : " + nonNullAndEmpty.test(""));
	     //System.out.println("For null            : " + nonNullAndEmpty.test(null)); // NullPointerException
		 
	     Predicate<String> p1 = s -> s.length() == 4;
	     Predicate<String> p2 = s -> s.startsWith("J");

	     Predicate<String> p3 = p1.xOr(p2);

	     System.out.println("For True = " + p3.test("True"));
	     System.out.println("For Julia = " + p3.test("Julia"));
	     System.out.println("For Java = " + p3.test("Java"));		 
	}

}
