package com.md.devoxx.day1;

import com.md.devoxx.day1.model.Person;
import com.md.devoxx.day1.util.Validator;

public class Play05_Validators {

	public static void main(String[] args) {
		
		Validator<Person> validator =
                Validator.<Person>firstValidate(p -> p.getLastName() == null, "name is null")
                        .thenValidate(p -> p.getAge() < 0, "age is negative")
                        .thenValidate(p -> p.getAge() > 150, "age is greater than 150");

		Person p1 = new Person("Stuart", "Pierce", 25);
        System.out.println(validator.validate(p1).get());
        Person p2 = new Person("Matt", "Blue", 200);
        //System.out.println(validator.validate(p2).get());
        Person p3 = new Person(null, "Hoops", 23);
        //System.out.println(validator.validate(p3).get());
        Person p4 = new Person("Stuart", "Pierce", -26);
        System.out.println(validator.validate(p4).get());
        
	}

}
