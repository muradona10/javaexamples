package com.md.devoxx.day1;

import java.util.ArrayList;
import java.util.List;

import com.md.devoxx.day1.util.Consumer;

public class Play01_Consumers {

	public static void main(String[] args) {
		
		Consumer<String> write = System.out::println;
		write.accept("Write first Consumer ...");
		write.accept("------------------------------------------------------------------------------");
		List<String> strings = new ArrayList<>();
		strings.add("one");
		Consumer<List<String>> c1 = l -> l.add("two");
		Consumer<List<String>> c2 = l -> l.add("three");
		Consumer<List<String>> c3 = c1.andThen(c2);
		c3.accept(strings);
		strings.forEach(i -> write.accept(i));
		write.accept("------------------------------------------------------------------------------");
		Consumer<List<String>> emptyList = List::clear; // l -> l.clear();
		emptyList.accept(strings);
		strings.forEach(i -> write.accept(i));
		write.accept("size of strings array: " + strings.size());
	}

}
