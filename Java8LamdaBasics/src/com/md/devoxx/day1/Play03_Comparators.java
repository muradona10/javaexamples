package com.md.devoxx.day1;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;

import com.md.devoxx.day1.model.Person;
//import com.md.devoxx.day1.util.Comparator;

public class Play03_Comparators {

	public static void main(String[] args) {
		
        Person michael = new Person("Jackson", "Michael", 51);
        Person michaelBis = new Person("Jackson", null, 51);
        Person rod = new Person("Rod", "Stewart", 71);
        Person paul = new Person("Paul", "McCartney", 74);
        Person mick = new Person("Mick", "Jagger", 73);
        Person mick2 = new Person("Mick", "Jagger", 75);
        Person jermaine = new Person("Jackson", "Jermaine", 61);

        Function<Person, String> getLastName = p -> p.getLastName();
        Function<Person, String> getFirstName = p -> p.getFirstName();
        Function<Person, Integer> getAge = p -> p.getAge();

        Comparator<Person> cmp = Comparator.comparing(getLastName)
                .thenComparing(getFirstName)
                .thenComparing(getAge);

        Comparator<Person> cmpNull = Comparator.nullsLast(cmp);
        List<Person> people = Arrays.asList(michael, rod, paul, mick, mick2, jermaine);
        people.sort(cmpNull);
        people.forEach(System.out::println);
        
        Comparator<String> cmp2 = Comparator.<String>nullsLast(Comparator.naturalOrder());
        List<String> people2 = Arrays.asList(michael.getLastName(), 
        		                             rod.getLastName(), 
        		                             paul.getLastName(), 
        		                             mick.getLastName(), 
        		                             mick2.getLastName(), 
        		                             jermaine.getLastName(), 
        		                             michaelBis.getLastName());
        people2.sort(cmp2);
        people2.forEach(System.out::println);
	}

}
