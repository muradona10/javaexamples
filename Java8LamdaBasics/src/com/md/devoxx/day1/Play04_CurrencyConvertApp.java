package com.md.devoxx.day1;

import java.time.LocalDate;

import com.md.devoxx.day1.util.CurrencyConverter;

public class Play04_CurrencyConvertApp {

	public static void main(String[] args) {
		
		LocalDate date = LocalDate.of(2018, 11, 5);
        CurrencyConverter converter = CurrencyConverter.of(date).from("EUR").to("GBP");
        
        double euroAmount = 100;
        double gbpAmount = converter.convert(euroAmount);
        System.out.println("Converted GBP: " + gbpAmount);
        
        converter = CurrencyConverter.of(date).from("EUR").to("USD");
        
        double usdAmount = converter.convert(euroAmount);
        System.out.println("Converted USD: " + usdAmount);
        
        converter = CurrencyConverter.of(date).from("TRY").to("CAD");
        double tryAmount = 1000;
        double cadAmount = converter.convert(tryAmount);
        System.out.println("Converted CAD: " + cadAmount);
        
	}

}
