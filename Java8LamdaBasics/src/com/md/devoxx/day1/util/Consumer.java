package com.md.devoxx.day1.util;

/**
 * Lambdas & Streams Day 1, Subject 1
 * @author MuratDemir
 *
 * @param <T>
 */

@FunctionalInterface
public interface Consumer<T> {
	
	/**
	 * This is the 1 abstract method of this Functional Interface
	 * @param t
	 */
	void accept(T t);
	
	/**
	 * default method, takes and one more consumer and apply accept method both of them
	 * @param other
	 * @return
	 */
	default Consumer<T> andThen(Consumer<T> other){
		return (T t) -> {
			this.accept(t);
			other.accept(t);
		};
	}

}
