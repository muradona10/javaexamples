package com.md.devoxx.day1.util;

import java.util.Objects;
import java.util.function.Function;

/**
 * Lambdas & Streams Day 1, Subject 3
 * @author MuratDemir
 *
 * @param <T>
 */

@FunctionalInterface
public interface Comparator<T> {
	
	/**
	 * This is the 1 abstract method of this Functional Interface
	 * @param t1
	 * @param t2
	 * @return
	 */
	int compare(T t1, T t2);
	
	/**
	 * Compare reverse
	 * @return
	 */
	default Comparator<T> reverse(){
		return (t1, t2) -> this.compare(t2, t1);
		
		/**
		 *  return (t1, t2) -> -1*this.compare(t1,t2) is wrong. do not use minus for comparing. ==> Integer.MINVALUE 
		 */
	}
	
	/**
	 * makes a new comparator for keyExractor
	 * @param keyExractor, is a java.util.function.Function.
	 * Function has apply method that takes T and returns U ==> (U apply(T))
	 * @return
	 */
	static <T,U extends Comparable<? super U>> Comparator<T> comparing(Function<T, U> keyExractor) {
		Objects.requireNonNull(keyExractor);
		
		return (t1, t2) -> {
			U u1 = keyExractor.apply(t1);
			U u2 = keyExractor.apply(t2);
			return u1.compareTo(u2);
		};
	}
	
	/**
	 * Combine comparators by using comparing method
	 * @param keyExtractor
	 * @return
	 */
	default <U extends Comparable<? super U>> Comparator<T> thenComparing(Function<T, U> keyExtractor) {
        Objects.requireNonNull(keyExtractor);

        return (t1, t2) -> {
            int cmp = this.compare(t1, t2);
            if (cmp == 0) {
                Comparator<T> other = comparing(keyExtractor);
                return other.compare(t1, t2);
            } else {
                return cmp;
            }
        };
    }
	
	/**
	 * Null comparator
	 * @param cmp
	 * @return
	 */
    static <T> Comparator<T> nullsLast(Comparator<T> cmp) {
        Objects.requireNonNull(cmp);
        
        return (t1, t2) -> {
            if (t1 == t2) {
                return 0;
            } else if (t1 == null) {
                return 42;
            } else if (t2 == null) {
                return -41;
            } else {
                return cmp.compare(t1, t2);
            }
        };
    }

}
