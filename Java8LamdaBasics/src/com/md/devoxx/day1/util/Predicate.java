package com.md.devoxx.day1.util;

/**
 * Lambdas & Streams Day 1, Subject 2
 * @author MuratDemir
 *
 * @param <T>
 */

@FunctionalInterface
public interface Predicate<T> {
	
	/**
	 * This is the 1 abstract method of this Functional Interface
	 * @param t
	 * @return true or false
	 */
	boolean test(T t);
	
	/**
	 * Negate a predicate
	 * @return
	 */
	default Predicate<T> negate(){
		return (T t) -> !this.test(t);
	}
	
	/**
	 * combine two predicate both of them must be true to be function returns true
	 * @param other
	 * @return
	 */
	default Predicate<T> and(Predicate<T> other) {
		return t -> this.test(t) && other.test(t);
	}

	/**
	 * xor 
	 * @param other
	 * @return
	 */
	default Predicate<T> xOr(Predicate<T> other){
		return t -> this.test(t) ^ other.test(t);
	}
}
