package com.md.java8lambdas;

@FunctionalInterface
public interface AddInterface {
	
	int add(int a, int b);
	
}
