package com.md.java8lambdas;

public class LambdaExamples {

	public static void main(String[] args) {
		
		/**
		 * java lambdas only works Functional Interfaces
		 * means includes only one and appropriate abstract function
		 */
		
		/**
		 * Runnable Example
		 * with anonymous inner class 
		 */
		Thread myAnonymousThread = new Thread(new Runnable() {
			
			@Override
			public void run() {
				System.out.println("Run with Anonymous Inner Class");
			}
		});
		myAnonymousThread.run();
		
		/**
		 * Runnable Example
		 * with lambda expressions
		 */
		Thread myLambdaThread = new Thread(() -> System.out.println("Run with Lambda Expressions"));
		myLambdaThread.run();
		
		/**
		 * Add Example with Functional Interface
		 */
		AddInterface addLambda = (int a, int b) -> a + b;
		System.out.println(addLambda.add(12, 36));

	}

}
