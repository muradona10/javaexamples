package com.md.java8lambdas.basicexamples.unit3;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import com.md.java8lambdas.basicexamples.unit1.Person;

public class More_002_Streams {

	public static void main(String[] args) {
		List<Person> people = Arrays.asList(new Person("Antoine", "Griezmann", 27), new Person("Kylian", "Mbapp�", 19),
				new Person("Paul", "Pogba", 25), new Person("Olivier", "Giroud", 31),
				new Person("Benjamin", "Pavard", 22), new Person("Hugo", "Lloris", 31));

		people.stream().filter(p->p.getFirstName().length()>5)
		               .forEach(System.out::println); 
		
		System.out.println("------------------------------------------");
		
		/*
		 * This is not terminate method, by using "." stream can go on
		 */
		Stream<Person> filter = people.stream().filter(p->p.getAge()>30);
		filter.forEach(System.out::println);
		
		System.out.println("------------------------------------------");
		
		/*
		 * This is a terminated method, stream ends with this.
		 */
		long count = people.stream().filter(p->p.getAge()<30).count();
		System.out.println("Count: " + count);
		
		
	}

}
