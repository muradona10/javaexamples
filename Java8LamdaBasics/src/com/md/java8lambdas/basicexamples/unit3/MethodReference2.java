package com.md.java8lambdas.basicexamples.unit3;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

import com.md.java8lambdas.basicexamples.unit1.Person;

public class MethodReference2 {

	public static void main(String[] args) {
		
		List<Person> people = Arrays.asList(
					new Person("Antoine", "Griezmann", 27),
					new Person("Kylian", "Mbapp�", 19),
					new Person("Paul", "Pogba", 25),
					new Person("Olivier", "Giroud", 31),
					new Person("Benjamin", "Pavard", 22),
					new Person("Hugo", "Lloris", 31)
				);
		
		
		/**
		 * Print All List
		 */
		System.out.println("********************************************************");
		printConditionally(people, p -> true, p -> System.out.println(p));
		
		/**
		 * If lambda has one argument and method has the same argument; then Method Reference can be used
		 * p -> method(p) === Class::method
		 */
		System.out.println("********************************************************");
		printConditionally(people, p -> true, System.out::println);
		
	}
	
	/**
	 * Instead of Condition interface we use Function Interface java supplied
	 * Predicate class is one of these interface. 
	 * In java.util.function package lots of Function Interfaces can seen
	 */
	private static void printConditionally(List<Person> people, Predicate<Person> predicate, Consumer<Person> consumer) {
		for(Person p : people) {
			if(predicate.test(p)) {
				consumer.accept(p);
			}
		}
	}
	

}

