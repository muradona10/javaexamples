package com.md.java8lambdas.basicexamples.unit3;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import com.md.java8lambdas.basicexamples.unit1.Person;

public class More_001_ForEach {

	public static void main(String[] args) {
		List<Person> people = Arrays.asList(new Person("Antoine", "Griezmann", 27), new Person("Kylian", "Mbapp�", 19),
				new Person("Paul", "Pogba", 25), new Person("Olivier", "Giroud", 31),
				new Person("Benjamin", "Pavard", 22), new Person("Hugo", "Lloris", 31));

		/*
		 * for loop, is sequential so we cannot use parallel programming 
		 */
		System.out.println("For loop..");
		for (int i = 0; i < people.size(); i++) {
			System.out.println(people.get(i));
		}
		
		/*
		 * for in loop, is also sequential so we cannot use parallel programming 
		 */
		System.out.println("For in loop..");
		for (Person p : people) {
			System.out.println(p);
		}
		
		/*
		 * Iterator, is also sequential so we cannot use parallel programming 
		 */
		System.out.println("Iterator..");
		Iterator<Person> iter = people.iterator();
		while(iter.hasNext()) {
			System.out.println(iter.next());
		}
		
		/*
		 * for each, is not sequential so we can use parallel programming 
		 * forEach(Consumer<? super T> action), Consumer is a Function Type
		 * and takes an argument and makes an operation with it
		 */
		System.out.println("For each loop..");
		people.forEach(System.out::println);
		
		
	}

}
