package com.md.java8lambdas.basicexamples.unit3;

public class MethodReference1 {

	public static void main(String[] args) {

		/**
		 * Lambda Expression Style
		 */
		Thread newThread = new Thread(() -> printMessage());
		newThread.start();
		
		/**
		 * Method Reference style
		 * If lambda has no argument and method has no argument as well; then Method Reference can be used
		 * () -> method() === Class:method
		 */
		Thread mrThread = new Thread(MethodReference1::printMessage);
		mrThread.start();
		
	}
	
	public static void printMessage() {
		System.out.println("Hello");
	}

}
