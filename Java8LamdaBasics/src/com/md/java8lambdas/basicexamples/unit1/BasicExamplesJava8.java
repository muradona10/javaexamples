package com.md.java8lambdas.basicexamples.unit1;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class BasicExamplesJava8 {
	
	public static void main(String[] args) {
		
		List<Person> people = Arrays.asList(
					new Person("Antoine", "Griezmann", 27),
					new Person("Kylian", "Mbapp�", 19),
					new Person("Paul", "Pogba", 25),
					new Person("Olivier", "Giroud", 31),
					new Person("Benjamin", "Pavard", 22),
					new Person("Hugo", "Lloris", 31)
				);
		
		
		/**
		 * Sort list by lastname
		 */
		Collections.sort(people, (p1, p2) -> p1.getLastName().compareToIgnoreCase(p2.getLastName()));
		
		/**
		 * Print All List
		 */
		System.out.println("********************************************************");
		printConditionally(people, p -> true);
		
		/**
		 * Print items that age lower than 30
		 */
		System.out.println("********************************************************");
		printConditionally(people, p -> p.getAge() < 30);
		
		/**
		 * Print items that age higher than 30
		 */
		System.out.println("********************************************************");
		printConditionally(people, p -> p.getAge() > 30);
		
	}
	
	private static void printConditionally(List<Person> people, Condition condition) {
		for(Person p : people) {
			if(condition.test(p)) {
				System.out.println(p.toString());
			}
		}
	}

}

