package com.md.java8lambdas.basicexamples.unit2;

import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;

public class BasicExamplesJava8_004_ExceptionHandlingAdvanced {

	public static void main(String[] args) {
		
		Integer [] numbers = {12,44,35,17,59,60};
		int key = 3;
		int exceptionKey = 0;
		
		process(numbers, key, wrapperLambda((x, y) -> System.out.println(x + " divided by " + y + " is: " + x / y)));
		
		process(numbers, exceptionKey, wrapperLambda((x, y) -> System.out.println(x + " divided by " + y + " is: " + x / y)));
		
		Double [] doubleNumbers = {11.5, 12.8, 44.6, 33.9};
		
		process(doubleNumbers, key, wrapperLambda((x, y) -> 
		      System.out.println(x + " divided by " + y + " is: " + (Double.parseDouble(x.toString()) / Double.parseDouble(y.toString())))));
	}


	/**
	 * BiConsumer<T,T> accept(obj, obj) takes 2 argument and return void
	 * @param <T>
	 * @param numbers
	 * @param key
	 * @param biConsumer, is an lambda expression to help coding flexibility
	 */
	private static <T> void process(T[] numbers, T key, BiConsumer<T, T> biConsumer) {
		for(T i : numbers) {
			biConsumer.accept(i, key);
		}
	}
	
	/**
	 * Isolate the exception from main lambda
	 * @param <T>
	 * @param object
	 * @return
	 */
	private static <T> BiConsumer<T, T> wrapperLambda(BiConsumer<T, T> wrapper) {
		return (a, b) -> {
			try {
				wrapper.accept(a, b);
			}catch (Exception e) {
				System.err.println("Exception is occured in wrapper lambda");
			}
		};
	}

}
