package com.md.java8lambdas.basicexamples.unit2;

public class BasicExamplesJava8_006_ThisReference {

	public static void main(String[] args) {
		
		//System.out.println(this); // this, is not usable in static context
		
		/**
		 * Anonymous Style
		 */
		BasicExamplesJava8_006_ThisReference b = new BasicExamplesJava8_006_ThisReference();
		b.doProcess(10, new Process2() {
			
			@Override
			public void process(int i) {
				System.out.println("In Anonymous Style");
				System.out.println(this); // this, is usable in anonymous method
			}
			
			@Override
			public String toString() {
				return "this, in the anonymous style";
			}
		});
		
		
		
		/**
		 * Lambda Style
		 */
		b.doProcess(10, i -> {
			System.out.println("In Lambda Expression");
			//System.out.println(this); // this, is not usable in lambda expression, because this part is in static context
		});
		
		/**
		 * Lambda Style by using not static part
		 */
		b.execute();
		
	}

	@Override
	public String toString() {
		return "this, in the lambda expression style";
	}
	
	public void doProcess(int i, Process2 p) {
		p.process(i);
	}
	
	public void execute() {
		doProcess(10, i -> {
			System.out.println("Non static lambda expression");
			System.out.println(this); // this, is usable in non static context
		});
	}
	
}

interface Process2{
	void process(int i);
}
