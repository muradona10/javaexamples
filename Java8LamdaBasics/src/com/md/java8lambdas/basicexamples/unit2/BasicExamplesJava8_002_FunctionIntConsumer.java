package com.md.java8lambdas.basicexamples.unit2;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

import com.md.java8lambdas.basicexamples.unit1.Person;

public class BasicExamplesJava8_002_FunctionIntConsumer {
	
	public static void main(String[] args) {
		
		List<Person> people = Arrays.asList(
					new Person("Antoine", "Griezmann", 27),
					new Person("Kylian", "Mbapp�", 19),
					new Person("Paul", "Pogba", 25),
					new Person("Olivier", "Giroud", 31),
					new Person("Benjamin", "Pavard", 22),
					new Person("Hugo", "Lloris", 31)
				);
		
		
		/**
		 * Sort list by lastname
		 */
		Collections.sort(people, (p1, p2) -> p1.getLastName().compareToIgnoreCase(p2.getLastName()));
		
		/**
		 * Print All List
		 */
		System.out.println("********************************************************");
		printConditionally(people, p -> true, p -> System.out.println(p));
		
		/**
		 * Print items that age lower than 30
		 */
		System.out.println("********************************************************");
		printConditionally(people, p -> p.getAge() < 30, p -> System.out.println(p.getFirstName()));
		
		/**
		 * Print items that age higher than 30
		 */
		System.out.println("********************************************************");
		printConditionally(people, p -> p.getAge() > 30, p -> System.out.println(p.getLastName()));
		
	}
	
	/**
	 * Instead of Condition interface we use Function Interface java supplied
	 * Predicate class is one of these interface. 
	 * In java.util.function package lots of Function Interfaces can seen
	 */
	private static void printConditionally(List<Person> people, Predicate<Person> predicate, Consumer<Person> consumer) {
		for(Person p : people) {
			if(predicate.test(p)) {
				consumer.accept(p);
			}
		}
	}

}

