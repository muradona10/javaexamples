package com.md.java8lambdas.basicexamples.unit2;


public class BasicExamplesJava8_005_ClosuresInLambdas {

	public static void main(String[] args) {
		
		int i = 60;
		int j = 30;
		
		/**
		 * Anonymous Style
		 */
		doProcess(i, new Process() {
			
			@Override
			public void process(int i) {
				// j = 45; - This is an error, variable that used is anonymous part must be final
				// Error Message: Local variable j defined in an enclosing scope must be final or effectively final
				int k = 50; // But we can define new local variable here
				k = 67; // And we can change it
				System.out.println(i+j+k);
			}
		});
		
		//j = 80; - This is an error, variable that used is anonymous part must be final
		
		/**
		 * Lambda Style
		 */
		doProcess(i, m -> System.out.println(i+j));
		
	}

	public static void doProcess(int i, Process p) {
		p.process(i);
	}

}

interface Process {
	void process(int i);
}
