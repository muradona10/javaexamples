package com.md.java8lambdas.basicexamples.unit2;

import java.util.function.BiConsumer;

public class BasicExamplesJava8_003_ExceptionHandling {

	public static void main(String[] args) {
		
		int [] numbers = {12,44,35,17,59,60};
		int key = 3;
		int exceptionKey = 0;
		
		process(numbers, key, (x, y) -> {
			      try {
			    	  System.out.println(x + " divided by " + y + " is: " + x / y);
			      }catch (ArithmeticException e) {
			    	  System.err.println("Aritmetic Exception is occured");
				}
		       });
		
		process(numbers, exceptionKey, (x, y) -> {
		      try {
		    	  System.out.println(x + " divided by " + y + " is: " + x / y);
		      }catch (ArithmeticException e) {
		    	  System.err.println("Aritmetic Exception is occured");
			}
	       });
	}

	/**
	 * BiConsumer<T,T> accept(obj, obj) takes 2 argument and return void
	 * @param numbers
	 * @param key
	 * @param biConsumer, is an lambda expression to help coding flexibility
	 */
	private static void process(int[] numbers, int key, BiConsumer<Integer, Integer> biConsumer) {
		for(int i : numbers) {
			biConsumer.accept(i, key);
		}
	}

}
