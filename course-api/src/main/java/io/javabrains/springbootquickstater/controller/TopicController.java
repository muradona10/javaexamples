package io.javabrains.springbootquickstater.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.javabrains.springbootquickstater.model.Topic;

@RestController
public class TopicController {
	
	@RequestMapping("/topics")
	public List<Topic> getAllTopics(){
		return Arrays.asList(
				
					new Topic("java", "Java", "Java SE, Java EE and Java ME courses"),
					new Topic("spring", "Spring", "Spring Core, Spring AOP, Spring Data ana Spring Boot Applications"),
					new Topic("python","Python","Machine Learning with Python"),
					new Topic("javascript","Javascript","Javascript Examples")
				
				);
	}

}
