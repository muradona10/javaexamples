package com.md.springData.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.md.springData.model.Circle;

/**
 * 
 * @author Murat Demir This class is responsible for the database connection and
 *         other database functions like query, insert, update etc.
 *
 */
@Component
public class JdbcDaoImpl {
	
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplate = new JdbcTemplate();

	public int getCircleCount() {
		String sql = "select count(*) from circle";
		int circleCount = jdbcTemplate.queryForObject(sql, Integer.class);
		return circleCount;
	}
	
	public String getCircleName(int circleId) {
		String sql = "select name from circle where id = ?";
		return jdbcTemplate.queryForObject(sql, new Object[] {circleId}, String.class);
	}
	
	public Circle getCircleById(int circleId) {
		String sql = "select * from circle where id = ?";
		return jdbcTemplate.query(sql, new Object[] {circleId}, new CircleMapper()).get(0);
	}
	
	public List<Circle> getAllCircles() {
		String sql = "select * from circle";
		return jdbcTemplate.query(sql, new CircleMapper());
	}
	
	private static final class CircleMapper implements RowMapper<Circle> {

		@Override
		public Circle mapRow(ResultSet resultSet, int rowNum) throws SQLException {
			
			int id = resultSet.getInt("ID");
			String name = resultSet.getString("NAME");
			Circle circle = new Circle(id,name);
			return circle;
		}

	}
	
	public DataSource getDataSource() {
		return dataSource;
	}

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		this.jdbcTemplate.setDataSource(this.dataSource);
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

}
