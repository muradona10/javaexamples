package com.md.spring.core;

import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

@Component
public class Circle implements Shape {
	
	@Value(value="12")
	private double radius;
	
	@Resource
	private Point center;
	
	/**
	 * MessageSource
	 */
	@Autowired
	private MessageSource messageSource;
	
	public void draw() {
		
		System.out.println(messageSource.getMessage("greeting", null, "Default Greeting", new Locale("en", "UK")));
		
		Object[] args = new Object[] {getCenter().getX(), getCenter().getY(), getRadius()};
		System.out.println(messageSource.getMessage("draw", args, "Mesaj Yok", new Locale("tr", "TR")));
		
	}
	
	public double getRadius() {
		return radius;
	}
	public void setRadius(double radius) {
		this.radius = radius;
	}
	
	public Point getCenter() {
		return center;
	}
	public void setCenter(Point center) {
		this.center = center;
	}
	
	@PostConstruct
	public void initializeCircle() {
		System.out.println("Initialization of Circle");
	}
	
	@PreDestroy
	public void destroyCircle() {
		System.out.println("Destruction of Circle");
	}

	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
}
