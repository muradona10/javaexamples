package com.md.spring.core;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class DrawApp {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		
		/**
		 * For using registerShutdownHook method
		 */
		AbstractApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
		/**
		 * For displaying destruction of an object
		 */
		context.registerShutdownHook();
		Shape shape = (Shape) context.getBean("circle");
		shape.draw();
	}
}
