package com.md.spring.core;

public class Circle implements Shape {
	
	private double radius;
	private Point center;
	
	public void draw() {
		System.out.println("Drawing Circle:");
		System.out.println("Center:(" + center.getX() + "," + center.getY() + ")");
		System.out.println("Radius:" + radius);
	}
	
	public double getRadius() {
		return radius;
	}
	public void setRadius(double radius) {
		this.radius = radius;
	}
	public Point getCenter() {
		return center;
	}
	public void setCenter(Point center) {
		this.center = center;
	}
	

}
