package com.md.spring.core;

public class Triangle implements Shape {
	
	private Point a;
	private Point b;
	private Point c;
	
	public void draw() {
		System.out.println("Drawing Triangle:");
		System.out.println("Poing x: (" + this.getA().getX() + "," + this.getA().getY() + ")");
		System.out.println("Poing y: (" + this.getB().getX() + "," + this.getB().getY() + ")");
		System.out.println("Poing z: (" + this.getC().getX() + "," + this.getC().getY() + ")");
		
	}
	
	public Point getA() {
		return a;
	}
	public void setA(Point a) {
		this.a = a;
	}
	public Point getB() {
		return b;
	}
	public void setB(Point b) {
		this.b = b;
	}
	public Point getC() {
		return c;
	}
	public void setC(Point c) {
		this.c = c;
	}
	
	

}
