package com.md.spring.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class DrawApp {

	public static void main(String[] args) throws IOException {
		
		ApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
		
		/**
		 * If file in src
		 */
		//File file = new File("src/AppConfig.properties");
		
		/**
		 * If file out src
		 */
		File file = new File("AppConfig.properties");

		FileInputStream fileInput;
		String beanName="";
		try {
			fileInput = new FileInputStream(file);
			Properties properties = new Properties();
			properties.load(fileInput);
			fileInput.close();
			beanName = properties.getProperty("shape");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		Shape shape = (Shape) context.getBean(beanName);
		shape.draw();
	}
}
