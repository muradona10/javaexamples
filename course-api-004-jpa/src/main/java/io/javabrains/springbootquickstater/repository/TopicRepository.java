package io.javabrains.springbootquickstater.repository;

import org.springframework.data.repository.CrudRepository;
import io.javabrains.springbootquickstater.model.Topic;

/**
 * This interface extends CrudRepository
 * to handle all common crud operations of tables
 * <Topic, String> means this interface used for Topic table, and primary key is String type
 * 
 * @author Murat Demir
 * 28.07.2018
 */
public interface TopicRepository extends CrudRepository<Topic, String>{

}
