package io.javabrains.springbootquickstater.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.javabrains.springbootquickstater.model.Topic;
import io.javabrains.springbootquickstater.service.TopicService;

@RestController
public class TopicController {
	
	@Autowired
	private TopicService topicService;
	
	/**
	 * @return : List of Topics
	 */
	@RequestMapping("/topics")
	public List<Topic> getAllTopics(){
		return topicService.getAllTopics();
	}
	
	/**
	 * {xx} means that this is a parameter value
	 * @PathVariable means that value in the curly braces in the uri is equal to function parameter
	 * @RequestMapping("/topics/{param}") - getTopic(@PathVariable("param") String id) is also true, but not recommended.
	 * @param id : Path Variable
	 * @return   : Topic which desired id
	 */
	@RequestMapping("/topics/{id}")
	public Topic getTopic(@PathVariable String id) {
		return topicService.getTopic(id);
	}
	
	/**
	 * Adding a topic into the topics list is a POST operation, 
	 * if we do not write the Request Method defaultly GET method works
	 * @RequestBody annotation turns parameter to the Topic object
	 * @param topic
	 */
	@RequestMapping(method=RequestMethod.POST, value="/topics")
	public void addTopic(@RequestBody Topic topic) {
		topicService.addTopic(topic);
	}
    
	/**
	 * For update request, method is PUT
	 * @param topic
	 * @param id
	 */
	@RequestMapping(method=RequestMethod.PUT, value="/topics/{id}")
	public void updateTopic(@RequestBody Topic topic, @PathVariable String id) {
		topicService.updateTopic(id, topic);
	}
	
	/**
	 * For delete requesti method is DELETE
	 * @param id
	 */
	@RequestMapping(method=RequestMethod.DELETE, value="/topics/{id}")
	public void deteleTopic(@PathVariable String id) {
		topicService.deleteTopic(id);
	}
}
