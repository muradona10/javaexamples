package io.javabrains.springbootquickstater.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.javabrains.springbootquickstater.model.Topic;
import io.javabrains.springbootquickstater.repository.TopicRepository;

@Service
public class TopicService {
	
	@Autowired
	private TopicRepository topicRepository;
	
	public List<Topic> getAllTopics() {
		List<Topic> topics = new ArrayList<>();
		/**
		 * java 8 lambda expression, method reference
		 * ( t -> topics.add(t) ) = topics::add
		 */
		topicRepository.findAll().forEach(topics::add);
		return topics;
	}
	
	public Topic getTopic(String id) {
		/**
		 * Returns only one topic
		 */
		return topicRepository.findOne(id);
	}

	public void addTopic(Topic topic) {
		/**
		 * If this topic is already exist in the db, then update it otherwise insert it
		 */
		topicRepository.save(topic);
	}

	public void updateTopic(String id, Topic topic) {
		/**
		 * If this topic is already exist in the db, then update it otherwise insert it
		 */
		topicRepository.save(topic);
	}

	public void deleteTopic(String id) {
		/**
		 * Delete topic that has given id
		 */
		topicRepository.delete(id);
	}

}
