package io.javabrains.springbootquickstater.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

import io.javabrains.springbootquickstater.model.Topic;

@Service
public class TopicService {
	
	private List<Topic> topics = Arrays.asList(
			
			new Topic("java", "Java", "Java SE, Java EE and Java ME courses"),
			new Topic("spring", "Spring", "Spring Core, Spring AOP, Spring Data ana Spring Boot Applications"),
			new Topic("python","Python","Machine Learning with Python"),
			new Topic("javascript","Javascript","Javascript Examples")
		
		);
	
	public List<Topic> getAllTopics() {
		return this.topics;
	}
	
	public Topic getTopic(String id) {
		/**
		 * Java 8 stream api features
		 * convert to stream, filter that to get desired "id"
		 */
		return this.topics.stream().filter(t -> t.getId().equals(id)).findFirst().get();
	}

}
