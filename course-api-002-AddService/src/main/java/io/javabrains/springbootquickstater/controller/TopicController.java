package io.javabrains.springbootquickstater.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.javabrains.springbootquickstater.model.Topic;
import io.javabrains.springbootquickstater.service.TopicService;

@RestController
public class TopicController {
	
	@Autowired
	private TopicService topicService;
	
	/**
	 * @return : List of Topics
	 */
	@RequestMapping("/topics")
	public List<Topic> getAllTopics(){
		return topicService.getAllTopics();
	}
	
	/**
	 * {xx} means that this is a parameter value
	 * @PathVariable means that value in the curly braces in the uri is equal to function parameter
	 * @RequestMapping("/topics/{param}") - getTopic(@PathVariable("param") String id) is also true, but not recommended.
	 * @param id : Path Variable
	 * @return   : Topic which desired id
	 */
	@RequestMapping("/topics/{id}")
	public Topic getTopic(@PathVariable String id) {
		return topicService.getTopic(id);
	}

}
