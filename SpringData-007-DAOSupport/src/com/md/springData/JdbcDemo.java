package com.md.springData;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.md.springData.dao.JdbcDaoImpl;
import com.md.springData.model.Circle;
import com.md.springData.model.Triangle;

public class JdbcDemo {

	public static void main(String[] args) {
		
		@SuppressWarnings("resource")
		ApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
		JdbcDaoImpl dao = context.getBean("jdbcDaoImpl",JdbcDaoImpl.class);
		
		dao.clearCircleTable();
		System.out.println("----------------------------------------------");
		dao.insertCircle(new Circle(1, "Circle 1"));
		dao.insertCircle(new Circle(2, "Circle 2"));
		dao.insertCircle(new Circle(3, "Circle 3"));
		List<Circle> circles = dao.getAllCircles();
		circles.stream().forEach(c->System.out.println(c.getName()));
		System.out.println("----------------------------------------------");
		dao.insertCircle(new Circle(4, "Circle 4"));
		dao.insertCircle(new Circle(5, "Circle 5"));
		dao.insertCircle(new Circle(3, "Third Circle"));
		dao.insertCircle(new Circle(6, "Circle 6"));
		circles = dao.getAllCircles();
		circles.stream().forEach(c->System.out.println(c.getName()));
		System.out.println("----------------------------------------------");
		dao.updateCircle(8, "Circle 8");
		dao.updateCircle(5, "Circle Five");
		circles = dao.getAllCircles();
		circles.stream().forEach(c->System.out.println(c.getName()));
		System.out.println("----------------------------------------------");
		dao.insertTriangle(new Triangle(1,"Triangle 1","Equivalent"));
		dao.insertTriangle(new Triangle(2,"Triangle 2","Equivalent"));
		dao.insertTriangle(new Triangle(3,"Triangle 3","Mix"));
		List<Triangle> triangles = dao.getAllTriangle();
		triangles.stream().forEach(t->System.out.println(t.getName() + " " + t.getType()));
		dao.clearTriangleTable();
	}

}
