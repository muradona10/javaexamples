package com.md.springData.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.md.springData.model.Circle;
import com.md.springData.model.Triangle;

/**
 * 
 * @author Murat Demir This class is responsible for the database connection and
 *         other database functions like query, insert, update etc.
 *
 */
public class JdbcDaoImpl extends JdbcDaoSupport {
	
	public int getCircleCount() {
		String sql = "select count(*) from circle";
		int circleCount = this.getJdbcTemplate().queryForObject(sql, Integer.class);
		return circleCount;
	}
	
	public String getCircleName(int circleId) {
		String sql = "select name from circle where id = ?";
		return this.getJdbcTemplate().queryForObject(sql, new Object[] {circleId}, String.class);
	}
	
	public Circle getCircleById(int circleId) {
		String sql = "select * from circle where id = ?";
		List<Circle> circles = this.getJdbcTemplate().query(sql, new Object[] {circleId}, new CircleMapper());
		if(circles!=null && circles.size()>0)
		   return circles.get(0);
		else return null;
	}
	
	public List<Circle> getAllCircles() {
		String sql = "select * from circle";
		return this.getJdbcTemplate().query(sql, new CircleMapper());
	}
	
	public void insertCircle(Circle circle) {
		Circle c = getCircleById(circle.getId());
		if(c == null) {
			String sql = "insert into circle(id, name) values(?, ?)";
			this.getJdbcTemplate().update(sql, new Object[] {circle.getId(), circle.getName()});
		}else {
			System.out.println("This circle is already exist in db!");
		}
	}
	
	public void updateCircle(int circleId, String newName) {
		Circle circle = getCircleById(circleId);
		if(circle != null) {
			String sql = "update circle set name = ? where id = ?";
			this.getJdbcTemplate().update(sql, new Object[] {newName, circleId});
		}else {
			System.out.println("Unable to find requested Circle!");
		}
	}
	
	public void clearCircleTable() {
		String sql = "delete from circle where 1=1";
		this.getJdbcTemplate().update(sql);
	}
	
	public void createTriangleTable() {
		String sql = "create table triangle (id integer, name varchar(50), type varchar(50))";
		this.getJdbcTemplate().execute(sql);
	}
	
	public void insertTriangle(Triangle triangle) {
		String sql = "insert into triangle(id, name, type) values(?, ?, ?)";
		this.getJdbcTemplate().update(sql, new Object[] {triangle.getId(), triangle.getName(),triangle.getType()});
	}
	
	public List<Triangle> getAllTriangle() {
		String sql = "select * from triangle";
		return this.getJdbcTemplate().query(sql, new TriangleMapper());
	}
	
	public void clearTriangleTable() {
		String sql = "delete from triangle where 1=1";
		this.getJdbcTemplate().update(sql);
	}
	
	private static final class CircleMapper implements RowMapper<Circle> {

		@Override
		public Circle mapRow(ResultSet resultSet, int rowNum) throws SQLException {
			
			int id = resultSet.getInt("ID");
			String name = resultSet.getString("NAME");
			Circle circle = new Circle(id,name);
			return circle;
		}

	}
	
	private static final class TriangleMapper implements RowMapper<Triangle> {

		@Override
		public Triangle mapRow(ResultSet resultSet, int rowNum) throws SQLException {
			
			int id = resultSet.getInt("ID");
			String name = resultSet.getString("NAME");
			String type = resultSet.getString("TYPE");
			Triangle triangle = new Triangle(id, name, type);
			return triangle;
		}

	}

}
