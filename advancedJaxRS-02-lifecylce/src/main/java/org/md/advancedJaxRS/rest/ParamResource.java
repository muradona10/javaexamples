package org.md.advancedJaxRS.rest;

import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("{pathParameter}/params")
public class ParamResource {

	@PathParam("pathParameter") private String path;
	@QueryParam("queryParameter") private String query;

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String test() {
		return "It works for params." + " Path Parameter: " + path + " Query Parameter: " + query;
	}
	
}
