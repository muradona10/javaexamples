package org.md.advancedJaxRS.rest;

import java.util.Calendar;
import java.util.Date;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("custommediatype")
public class CustomMediaTypeResource {
	
	@GET
	@Produces({MediaType.TEXT_PLAIN,"text/shortdate"})
	public Date testMessageBodyWriter() {
		return Calendar.getInstance().getTime();
	}

}
