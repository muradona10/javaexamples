package org.md.advancedJaxRS.rest;

import java.util.Calendar;
import java.util.Date;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("messagebody")
public class MessageBodyWriterResource {
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public Date testMessageBodyWriter() {
		return Calendar.getInstance().getTime();
	}

}
