package com.md.spring.core;

public interface Shape {
	
	void draw(); // it is equal to public void draw()

}
