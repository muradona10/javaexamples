package com.md.spring.core;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class Circle implements Shape {
	
	private double radius;
	
	@Resource
	private Point center;
	
	public void draw() {
		System.out.println("Drawing Circle:");
		System.out.println("Center:(" + center.getX() + "," + center.getY() + ")");
		System.out.println("Radius:" + radius);
	}
	
	public double getRadius() {
		return radius;
	}
	public void setRadius(double radius) {
		this.radius = radius;
	}
	
	public Point getCenter() {
		return center;
	}
	public void setCenter(Point center) {
		this.center = center;
	}
	
	@PostConstruct
	public void initializeCircle() {
		System.out.println("Initialization of Circle");
	}
	
	@PreDestroy
	public void destroyCircle() {
		System.out.println("Destruction of Circle");
	}
	
}
