package com.md.springAOP.service;

import com.md.springAOP.annotation.Loggable;
import com.md.springAOP.model.Circle;
import com.md.springAOP.model.Triangle;

public class ShapeService {
	
	private Circle circle;
	private Triangle triangle;
	
	public Circle getCircle() {
		return circle;
	}
	
	@Loggable
	public void setCircle(Circle circle) {
		this.circle = circle;
	}
	
	public Triangle getTriangle() {
		return triangle;
	}
	
	public void setTriangle(Triangle triangle) {
		this.triangle = triangle;
	}
	
	

}
