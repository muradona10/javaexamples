package com.md.springAOP.aspect;

import java.util.Arrays;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;


@Aspect
@Component
public class LoggingAspect {
	
	@Around("@annotation(com.md.springAOP.annotation.Loggable)")
	public Object argumentsMethods(ProceedingJoinPoint pjp) {
		
		Object returnVal = null;
		
		try {
			System.out.println("Before Advice Run Area");
			pjp.proceed();
			System.out.println("Target Method: " + pjp.getSignature().getName());
			System.out.println("Parameters   : " + Arrays.toString(pjp.getArgs()));
			System.out.println("AfterReturning Advice Run Area");
		} catch (Throwable e) {
			System.out.println("AfterThrowing Advice Run Area");
		}
		
		System.out.println("After Advice Run Area");
		return returnVal;
		
	}
	

	
}
