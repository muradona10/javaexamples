package com.md.springAOP;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.md.springAOP.model.Circle;
import com.md.springAOP.service.ShapeService;

public class AopMain {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		
		ApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
		ShapeService shapeService = context.getBean("shapeService", ShapeService.class);
		//ShapeService shapeService = (ShapeService) context.getBean("shapeService");
		Circle circle = shapeService.getCircle();
		shapeService.setCircle(circle);
	}

}
