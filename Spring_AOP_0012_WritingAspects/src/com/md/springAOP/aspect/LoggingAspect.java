package com.md.springAOP.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggingAspect {
	
	@Before("execution(public String getName())")
	public void LoggingAdvice() {
		System.out.println("[Logging Advice]:Get Method is calling..!");
	}

	 /*
	  * Sadece Circle'�n getName metodu �al��t���nda Aspect �al��s�n istiyorsak
	  * Pointcut a�a��daki gibi olmal�:
	  * @Before("execution(public String com.md.springAOP.model.Circle.getName())")
	  * */
}
