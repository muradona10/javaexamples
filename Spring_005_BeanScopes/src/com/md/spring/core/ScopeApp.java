package com.md.spring.core;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ScopeApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");

		System.out.println("Singleton Scope:");
		for (int i = 0; i < 5; i++) {
			Point p = (Point) context.getBean("point");
		}
		
		System.out.println("Prototype Scope:");
		for (int i = 0; i < 5; i++) {
			Point2 p = (Point2) context.getBean("point2");
		}
	}
}
