package com.md.spring.core;

public class Point2 {
	
	private int x;
	private int y;
	
	public Point2() {
		System.out.println("Point2: " + this.toString());
	}
	
	public Point2(int x, int y) {
		this.x = x;
		this.y = y;
		System.out.println("Point2: " + this.toString() + "(" + this.x + "," + this.y + ")");
	}
	
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	
	

}
