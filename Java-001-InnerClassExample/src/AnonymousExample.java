
public class AnonymousExample {

	interface HelloWorld {
		public void greet();

		public void greetSomeOne(String name);
	}

	public void sayHello() {

		/*
		 * This is a local class
		 */
		class EnglishGreeting implements HelloWorld {

			String name = "world";

			@Override
			public void greet() {
				greetSomeOne(name);
			}

			@Override
			public void greetSomeOne(String name) {
				this.name = name;
				System.out.println("Hello " + name);
			}

		}
		EnglishGreeting englishGreeting = new EnglishGreeting();
	
		/*
		 * This is an anonymous class
		 */
		HelloWorld frenchGreeting = new HelloWorld() {
			String name = "tout la monde";
			@Override
			public void greetSomeOne(String name) {
				System.out.println("Salut " + name);
			}
			
			@Override
			public void greet() {
				greetSomeOne(name);
			}
		};
		
		/*
		 * This is an another anonymous class
		 */
		HelloWorld turkishGreeting = new HelloWorld() {
			String name = "d�nya";
			@Override
			public void greetSomeOne(String name) {
				System.out.println("Merhaba " + name);
			}
			
			@Override
			public void greet() {
				greetSomeOne(name);
			}
		};
		
		englishGreeting.greet();
		frenchGreeting.greet();
		turkishGreeting.greet();
	}

	public static void main(String[] args) {
		AnonymousExample ae = new AnonymousExample();
		ae.sayHello();
	}

}
