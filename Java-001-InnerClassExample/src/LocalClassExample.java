
public class LocalClassExample {

	public static void validatePhoneNumbers(String pn1, String pn2) {

		final int numberLength = 10;

		class PhoneNumber {

			String formattedPhoneNumber = null;
			PhoneNumber(String phoneNumber){
                // numberLength = 7;
                String currentNumber = phoneNumber.replaceAll("-", "");
                if (currentNumber.length() == numberLength)
                    formattedPhoneNumber = currentNumber;
                else
                    formattedPhoneNumber = null;
            }
			

			public String getNumber() {
				return formattedPhoneNumber;
			}
		}

		PhoneNumber phoneNumber1 = new PhoneNumber(pn1);
		PhoneNumber phoneNumber2 = new PhoneNumber(pn2);

		if (phoneNumber1.getNumber() != null) {
			System.out.println(pn1 + " is valid");
		} else {
			System.out.println(pn1 + " is invalid");
		}

		if (phoneNumber2.getNumber() != null) {
			System.out.println(pn2 + " is valid");
		} else {
			System.out.println(pn2 + " is invalid");
		}

	}

	public static void main(String[] args) {
		validatePhoneNumbers("123-456-7890", "456-7890");
	}

}
