package com.md.javaexamples.enumaration;

public class EnumExample2 {

	public static void main(String... args) {
		
		Planet[] planets = Planet.values();
		
		for(Planet p : planets) {
			double earthWeight = 68.7;
	        double mass = earthWeight/Planet.EARTH.surfaceGravity();
			System.out.printf("Your weight in planet %-12s is % .1f%n" , p, p.surfaceWeight(mass));
		}
		
	}

}
