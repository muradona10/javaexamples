package com.md.javaexamples.enumaration;

public class EnumExample1 {
	
	Day day;
	
	EnumExample1(Day day){
		this.day = day;
	}
	
	public void printDayYouLiked() {
		switch (day) {
		case MONDAY:
			System.out.println("I hate from " + Day.MONDAY);
			break;
		case TUESDAY:
			System.out.println("I do not like " + Day.TUESDAY);
			break;
		case WEDNESDAY:
			System.out.println("I like " + Day.WEDNESDAY);
			break;
		case THIRSDAY:
			System.out.println("I like also " + Day.TUESDAY);
			break;
		case FRIDAY:
			System.out.println("I love " + Day.FRIDAY);
			break;
		case SATURDAY:
			System.out.println("I love very much " + Day.SATURDAY);
			break;
		case SUNDAY:
			System.out.println("I love very very much " + Day.SUNDAY);
			break;
		default:
			break;
		}
	}

	public static void main(String... args) {
		
		EnumExample1 monday = new EnumExample1(Day.MONDAY);
		monday.printDayYouLiked();
		EnumExample1 wednesday = new EnumExample1(Day.WEDNESDAY);
		wednesday.printDayYouLiked();
		EnumExample1 sunday = new EnumExample1(Day.SUNDAY);
		sunday.printDayYouLiked();
		
		
	}
	

}
