import java.util.Iterator;

public class InnerClassExample {
	
	private final static int SIZE = 20;
	private int[] intArray = new int[SIZE];

	public InnerClassExample() {
		for(int i=0;i<SIZE-1;i++) {
			intArray[i] = i;
		}
	}
	
	interface DataStructureIterator extends Iterator<Integer>{};
	
	/**
	 * This is an inner class to identify only even numbers in a list
	 */
	private class EvenIterator implements DataStructureIterator{
		
		private int nextIndex = 0;

		@Override
		public boolean hasNext() {
			// TODO Auto-generated method stub
			return nextIndex <= SIZE-1;
		}

		@Override
		public Integer next() {
			// TODO Auto-generated method stub
			Integer retValue = Integer.valueOf(intArray[nextIndex]);
			nextIndex = nextIndex + 2;
			return retValue;
		}
		
	}
	
	public void printEven() {
		DataStructureIterator it = this.new EvenIterator();
		while(it.hasNext()) {
			System.out.println(it.next());
		}
	}
	
	
	public static void main(String[] args) {
		
		InnerClassExample innerClass = new InnerClassExample();
		innerClass.printEven();
		
	}

}
