package org.md.advancedJaxRS.rest;

import java.io.IOException;
import java.util.List;
import java.util.StringTokenizer;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import com.sun.xml.internal.messaging.saaj.util.Base64;


@Provider
public class SecurityFilter implements ContainerRequestFilter {
	
	private static final String AUTHORIZATION_HEADER_KEY = "Authorization"; // Basic Auth header key
	private static final String AUTHORIZATION_HEADER_PREFIX = "Basic"; // Basic Auth prefix
	private static final String SECURED_URL_PREFIX = "secured"; // uri resource path

	public void filter(ContainerRequestContext requestContext) throws IOException {
		
		if(requestContext.getUriInfo().getPath().contains(SECURED_URL_PREFIX)) { // control only demanded resource path.
			
			List<String> authHeader = requestContext.getHeaders().get(AUTHORIZATION_HEADER_KEY);
			if(authHeader != null && authHeader.size() > 0) { // If any author header exist then control it
				String authToken = authHeader.get(0).replaceFirst(AUTHORIZATION_HEADER_PREFIX, ""); // remove Basic string from header text
				String decodedString = Base64.base64Decode(authToken); // convert from Base64 to String
				StringTokenizer tokenizer = new StringTokenizer(decodedString, ":");
				String username = tokenizer.nextToken(); // get username
				String password = tokenizer.nextToken(); // get password
				if("username".equals(username) && "password".equals(password)) {
					return;
				}
			}
			Response unAuthorizedStatus = Response.status(Response.Status.UNAUTHORIZED)  // unauthorized user 
			                                      .entity("User can not access the resourse..!")
			                                      .build();
			
			requestContext.abortWith(unAuthorizedStatus); // abort the request
			
		}
		
	}

}
