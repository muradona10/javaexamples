package com.md.springAOP.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggingAspect {
	
	@Pointcut("execution(public * get*())")
	public void allGetters() {}

	/**
	 * T�m getter metodlardan �nce �al��mas�n� istiyoruz
	 * Bu y�zden method sadece get ile ba�las�n ve d�n�� tipi ne olursa olsun diyelim
	 */
	@Before("allGetters()")
	public void LoggingAdvice() {
		System.out.println("[Logging Advice]:Get Method is calling..!");
	}
	
	@Before("allGetters()")
	public void SecondAdvice() {
		System.out.println("[Second Advice]:Get Method is calling..!");
	}
	
	// execution(* * get*()) --> d�n�� tipi �nemsiz get ile ba�la�an herhangi bir method
	
	// e�er get*(*) yazsayd�k yukar�daki advice bizim s�n�flar i�in �al��mazd�, ��nk� bizim 
	// s�n�flar�m�z�n get ile ba�layan ve arg�man alan methodlar� yok
	// bu ifade get ile ba�layan ve arg�man alan method i�eren s�n�flar i�in �al���r
	// get*()   -> 0 arg�man
	// get*(*)  -> 1 veya daha fazla arg�man
	// get*(..) -> 0 veya daha fazla arg�man
	
	// public * com.md.springAOP.model.*.get*(..)
	// com.md.springAOP.model alt�ndaki s�n�flardan get ile ba�layan 0 veya daha fazla arg�man alan
	// d�n�� de�eri �nemsiz ama public olan methodlar i�in �al��
	

}
