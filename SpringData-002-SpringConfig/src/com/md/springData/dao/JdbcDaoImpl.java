package com.md.springData.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.md.springData.model.Circle;

/**
 * 
 * @author Murat Demir This class is responsible for the database connection and
 *         other database functions like query, insert, update etc.
 *
 */
@Component
public class JdbcDaoImpl {
	
	@Autowired
	private DataSource dataSource;

	public Circle getCircleById(int circleId) {

		Connection conn = null;
		Circle circle = null;

		try {
			conn = dataSource.getConnection(); // database connection with spring

			// query string 
			PreparedStatement ps = conn.prepareStatement("select * from circle where id = ?");
			ps.setInt(1, circleId);

			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				circle = new Circle(circleId, rs.getString("name"));
			}
			rs.close();
			ps.close();
		} catch (Exception e) {
			new RuntimeException(e);
		} finally {
			try {
				conn.close();
			}catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return circle;
	}
	
	
	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

}
