package com.md.spring.core;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class Triangle implements ApplicationContextAware, BeanNameAware {
	
	private Point a;
	private Point b;
	private Point c;
	private ApplicationContext context = null;
	
	public void drawTriangle() {
		
		System.out.println("Poing x: (" + this.getA().getX() + "," + this.getA().getY() + ")");
		System.out.println("Poing y: (" + this.getB().getX() + "," + this.getB().getY() + ")");
		System.out.println("Poing z: (" + this.getC().getX() + "," + this.getC().getY() + ")");
		
	}
	
	public Point getA() {
		return a;
	}
	public void setA(Point a) {
		this.a = a;
	}
	public Point getB() {
		return b;
	}
	public void setB(Point b) {
		this.b = b;
	}
	public Point getC() {
		return c;
	}
	public void setC(Point c) {
		this.c = c;
	}

	@Override
	public void setBeanName(String beanName) {
		System.out.println("BeanName:" + beanName);
	}

	@Override
	public void setApplicationContext(ApplicationContext context) throws BeansException {
		System.out.println("Setting Application Context:" + context.toString());
		this.context = context;
	}
	
}
