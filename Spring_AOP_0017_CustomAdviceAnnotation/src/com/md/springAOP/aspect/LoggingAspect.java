package com.md.springAOP.aspect;

import java.util.Arrays;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;


@Aspect
@Component
public class LoggingAspect {
	
	/**
	 * @param radius
	 * @param newName
	 * Metoda ge�irdi�imiz isimler ile a�a��da kulland�klar�m�z ayn� olmal�...!
	 */
	@Around("args(radius, newName)")
	public Object argumentsMethods(ProceedingJoinPoint pjp, double radius,String newName) {
		
		Object returnVal = null;
		
		try {
			System.out.println("Before Advice Run Area");
			returnVal = pjp.proceed();
			System.out.println("Target Method: " + pjp.getSignature().getName());
			System.out.println("Parameters   : " + Arrays.toString(pjp.getArgs()));
			System.out.println("AfterReturning Advice Run Area");
		} catch (Throwable e) {
			System.out.println("AfterThrowing Advice Run Area");
		}
		
		System.out.println("After Advice Run Area");
		System.out.println("returnVal: " + returnVal.toString());
		return returnVal;
		
	}
	

	
}
