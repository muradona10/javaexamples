package com.md.springAOP.model;

public class Circle {
	
	public String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return this.name;
	}
	
	public String printCircle(double radius, String newName) {
		return "Name:" + newName + " radius:" + radius;
	}
	
	public String printCircleWithException(double radius, String newName) {
		throw(new RuntimeException());
	}

}
