package io.javabrains.springbootquickstater.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import io.javabrains.springbootquickstater.model.Course;

/**
 * This interface extends CrudRepository
 * to handle all common crud operations of tables
 * <Course, String> means this interface used for Course table, and primary key is String type
 * 
 * @author Murat Demir
 * 28.07.2018
 */
public interface CourseRepository extends CrudRepository<Course, String>{
	
	public List<Course> findByTopicId(String topicId);
	
	public List<Course> findByName(String name);
	
	public List<Course> findByDescription(String description);
	
	/**
	 * Spring JPA automatically implement these functions
	 * findBy[PropertyName of the Class] : CamelCase written
	 */

}
