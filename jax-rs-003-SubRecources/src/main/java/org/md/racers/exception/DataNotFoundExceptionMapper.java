package org.md.racers.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.md.racers.model.ErrorMessage;

@Provider // register for jaxRs
public class DataNotFoundExceptionMapper implements ExceptionMapper<DataNotFoundException> {

	@Override
	public Response toResponse(DataNotFoundException dataNotFoundException) {
		ErrorMessage errorMessage = new ErrorMessage(dataNotFoundException.getMessage(), 404, "http://google.com.tr");
		return Response.status(Status.NOT_FOUND)
				       .entity(errorMessage)
				       .build();
	}

}
