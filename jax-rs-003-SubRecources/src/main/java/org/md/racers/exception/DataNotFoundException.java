package org.md.racers.exception;

public class DataNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2895477537166502307L;

	public DataNotFoundException(String message) {
		super(message);
	}
	
	

}
