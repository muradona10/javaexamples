package org.md.racers.db;

import java.util.HashMap;
import java.util.Map;

import org.md.racers.model.Racer;

public class Database {
	
	private static Map<Long, Racer> racers = new HashMap<>();
	
	public static Map<Long, Racer> getRacers(){
		return racers;
	}

}
