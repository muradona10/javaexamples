package org.md.racers.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement
public class Racer {

	private long racerId;
	private String racerName;
	private String teamName;
	private Date racerBirthDate;
	private Map<Long,Comment> comments = new HashMap<>();
	private List<Link> links = new ArrayList<>();
	
	public Racer() {
		
	}
	
	public Racer(long racerId, String racerName, String teamName, Date racerBirthDate) {
		super();
		this.racerId = racerId;
		this.racerName = racerName;
		this.teamName = teamName;
		this.racerBirthDate = racerBirthDate;
	}
	
	public long getRacerId() {
		return racerId;
	}
	public void setRacerId(long racerId) {
		this.racerId = racerId;
	}
	public String getRacerName() {
		return racerName;
	}
	public void setRacerName(String racerName) {
		this.racerName = racerName;
	}
	public String getTeamName() {
		return teamName;
	}
	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}
	public Date getRacerBirthDate() {
		return racerBirthDate;
	}
	public void setRacerBirthDate(Date racerBirthDate) {
		this.racerBirthDate = racerBirthDate;
	}

	@XmlTransient // for conversion to xml
	public Map<Long, Comment> getComments() {
		return comments;
	}

	public void setComments(Map<Long, Comment> comments) {
		this.comments = comments;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}
	
	public void addLink(String url, String rel) {
		Link link = new Link();
		link.setUrl(url);
		link.setRel(rel);
		links.add(link);
	}
	
	
	
}
