package org.md.racers.model;

public class Comment {
	
	private long commentId;
	private String commandText;
	private String author;
	private long racerId;
	
	public Comment(long commentId, String commandText, String author, long racerId) {
		super();
		this.commentId = commentId;
		this.commandText = commandText;
		this.author = author;
		this.racerId = racerId;
	}
	
	public Comment() {}
	
	public long getCommentId() {
		return commentId;
	}
	public void setCommentId(long commentId) {
		this.commentId = commentId;
	}
	public String getCommandText() {
		return commandText;
	}
	public void setCommandText(String commandText) {
		this.commandText = commandText;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}

	public long getRacerId() {
		return racerId;
	}

	public void setRacerId(long racerId) {
		this.racerId = racerId;
	}
	
	
	

}
