package org.md.racers.service;

import java.util.Collection;

import org.md.racers.model.Comment;

public class CommentService {
	
	public RacerService racerService = new RacerService();
	
	public Collection<Comment> getAllCommentsForARacer(long racerId){
		return racerService.getARacer(racerId).getComments().values();
	}

	public Comment addComment(long racerId, Comment comment) {
		racerService.getARacer(racerId).getComments().put(comment.getCommentId(), comment);
		return comment;
	}
	
	public Comment getASpecificComment(long racerId, long commentId) {
		return racerService.getARacer(racerId).getComments().get(commentId);
	}
	
	public Comment updateComment(Comment comment, long racerId, long commentId) {
		Comment c = getASpecificComment(racerId, commentId);
		if(c!=null) {
			racerService.getARacer(racerId).getComments().replace(c.getCommentId(), c, comment);
			return comment;
		}
		return null;
	}
	
	public boolean deleteComment(Comment comment, long racerId, long commentId) {
		Comment c = getASpecificComment(racerId, commentId);
		if(c!=null) {
			racerService.getARacer(racerId).getComments().remove(c.getCommentId());
			return true;
		}
		return false;
	}

}
