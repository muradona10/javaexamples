package org.md.racers.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.md.racers.exception.DataNotFoundException;
import org.md.racers.model.Comment;
import org.md.racers.model.Racer;

@SuppressWarnings("deprecation")
public class RacerService {
	
	public List<Racer> dummyRacers = Arrays.asList(
		new Racer(1, "Valentino Rossi", "Yamaha", new Date(1987, 1, 10)),
		new Racer(2, "Maverick Vinales", "Yamaha", new Date(1991, 10, 25)),
		new Racer(3, "Andrea Dovizioso", "Ducati", new Date(1985, 10, 12)),
		new Racer(4, "Marc Marquez", "Honda", new Date(1991, 6, 21)));
	
	public static List<Racer> racers = new ArrayList<>();
	
	public RacerService() {
		
		Map<Long, Comment> comments1 = new HashMap<Long, Comment>();
		Map<Long, Comment> comments2 = new HashMap<Long, Comment>();
		
		if(racers==null || racers.isEmpty()) {
			comments1.put((long) 1, new Comment(1, "Rossi is alwalys the best", "valeyellow46funPage", 1));
			comments1.put((long) 3, new Comment(3, "ValeYellow46 *****", "valeyellow46funPage", 1));
			comments2.put((long) 2, new Comment(2, "Marc Marques is a coward", "valeyellow46funPage" ,4));
			dummyRacers.get(0).setComments(comments1);
			dummyRacers.get(3).setComments(comments2);
		    racers.addAll(dummyRacers);
		}
	}
	
	public List<Racer> getAllRacers(){
		return racers;
	}
	
	public List<Racer> getRacersByTeam(String teamName){
		return racers.stream().filter(r -> r.getTeamName().equals(teamName)).collect(Collectors.toList());
	}
	
	public List<Racer> getRacersPage(int startIndex, int size){
		if(startIndex+size<=racers.size())
		   return racers.subList(startIndex, startIndex+size);
		return null;
	}

	public Racer addRacer(Racer racer) {
		racers.add(racer);
		return racer;
	}
	
	public Racer getARacer(long id) {
		Racer racer;
		try {
			racer = racers.stream().filter(f -> id == f.getRacerId()).findFirst().get();
		} catch (Exception e) {
			racer = null;
			e.printStackTrace();
		}
		if(racer==null) {
			throw new DataNotFoundException("Racer with id: " + id + " is not found!");
		}
		return racer;
	}
	
	public Racer updateRacer(Racer racer, long id) {
		Racer r = getARacer(id);
		if(r!=null) {
			for(int i=0;i<racers.size();i++) {
				if(racers.get(i).getRacerId() == id) {
					racers.remove(i);
					racers.add(i, racer);
					return racer;
				}
			}
		}
		return null;
	}
	
	public boolean deleteRacer(Racer racer, long id) {
		Racer r = getARacer(id);
		if(r!=null) {
			for(int i=0;i<racers.size();i++) {
				if(racers.get(i).getRacerId() == id) {
					racers.remove(i);
					return true;
				}
			}
		}
		return false;
	}

}
