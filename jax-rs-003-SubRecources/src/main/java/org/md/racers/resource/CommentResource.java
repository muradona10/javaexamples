package org.md.racers.resource;

import java.net.URI;
import java.util.Collection;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.md.racers.model.Comment;
import org.md.racers.model.ErrorMessage;
import org.md.racers.service.CommentService;

@Path("/") // it is optional for sub resources
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class CommentResource {
	
	CommentService commentService = new CommentService();
	
	/*
	 * Use WebApplicationException for handling exceptions
	 */
	@GET
	public Collection<Comment> getCommentsForARacer(@PathParam("racerId") long racerId){
		
		if(racerId == 0) {
			throw new WebApplicationException(Response.status(Status.NOT_FOUND)
					                                  .entity(new ErrorMessage("Not found racerId: "+racerId, 404, "http://www.google.com.tr"))
					                                  .build());
		}
		
		Collection<Comment> comments = commentService.getAllCommentsForARacer(racerId);
		if(comments==null ||comments.size()==0) {
			throw new NotFoundException(Response.status(Status.NOT_FOUND)
                    .entity(new ErrorMessage("No comments for racerId: "+racerId, 404, "http://www.google.com.tr"))
                    .build());
		}
		
		return comments;
	}
	
	@GET
	@Path("/{commentId}")
	public Response getComment(@PathParam("racerId") long racerId, @PathParam("commentId") long commentId) {
		Comment aSpecificComment = commentService.getASpecificComment(racerId, commentId);
		return Response.ok(aSpecificComment)
				       .build();
	}
	
	@POST
	public Response addComment(@PathParam("racerId") long racerId, Comment comment, @Context UriInfo uriInfo) {
		
		Comment newComment = commentService.addComment(racerId, comment); // get comment entity to add on the Response
		String commentId = String.valueOf(newComment.getCommentId()); // get commentId and convert it to a String to add as a path parameter
		URI uri = uriInfo.getAbsolutePathBuilder().path(commentId).build(); // it is equal to uriInfo.getAbsolutePath and add commentId and convert all of these to URI
		
		return Response.created(uri) // uri
				       .entity(newComment) // entity
				       .build();
	}
	
	@PUT
	@Path("/{commentId}")
	public Comment updateComment(Comment comment, @PathParam("racerId") long racerId, @PathParam("commentId") long commentId) {
		return commentService.updateComment(comment, racerId, commentId);
	}
	
	@DELETE
	@Path("/{commentId}")
	public boolean deleteComment(Comment comment, @PathParam("racerId") long racerId, @PathParam("commentId") long commentId) {
		return commentService.deleteComment(comment, racerId, commentId);
	}

}
