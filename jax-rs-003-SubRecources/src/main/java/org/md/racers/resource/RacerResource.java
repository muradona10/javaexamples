package org.md.racers.resource;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import org.md.racers.model.Racer;
import org.md.racers.service.RacerService;

@Path("/racers")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
// @Produces(value= {MediaType.APPLICATION_JSON,MediaType.TEXT_XML}), we can write multiple media type
public class RacerResource {
	
	RacerService racerService = new RacerService();
	
	@GET
	public List<Racer> getRacers(@QueryParam(value="teamName") String teamName,
			                     @QueryParam(value="startIndex") int startIndex,
			                     @QueryParam(value="size") int size){
		if(teamName!=null)
			return racerService.getRacersByTeam(teamName);
		if(startIndex>0 && size>0)
			return racerService.getRacersPage(startIndex, size);
		return racerService.getAllRacers();
	}
	
	/**
	 * HATEOS implemented
	 * @param id
	 * @return
	 */
	@GET
	@Path("/{racerId}")
	@Produces(MediaType.APPLICATION_JSON) // we do not need because all class produces json
	public Racer getRacerForJson(@PathParam("racerId") Integer id, @Context UriInfo uriInfo) {
		System.out.println("Json Method Called..!");
		Racer racer = racerService.getARacer(id);
		racer.addLink(gerUriForSelf(uriInfo, racer),"self");
		racer.addLink(gerUriForComments(uriInfo, racer),"comments");
		return racer;
	}
	
	@GET
	@Path("/{racerId}")
	@Produces(MediaType.TEXT_XML) // override produces
	public Racer getRacerForXml(@PathParam("racerId") Integer id, @Context UriInfo uriInfo) {
		System.out.println("XML Method Called..!");
		Racer racer = racerService.getARacer(id);
		racer.addLink(gerUriForSelf(uriInfo, racer),"self");
		racer.addLink(gerUriForComments(uriInfo, racer),"comments");
		return racer;
	}

	@POST
	public Racer addRacer(Racer racer) {
		return racerService.addRacer(racer);
	}
	
	@PUT
	@Path("/{racerId}")
	public Racer updateRacer(Racer racer, @PathParam("racerId") Integer id) {
		return racerService.updateRacer(racer, id);
	}
	
	@DELETE
	@Path("/{racerId}")
	public boolean deleteRacer(Racer racer, @PathParam("racerId") Integer id) {
		return racerService.deleteRacer(racer, id);
	}
	
	@Path("/{racerId}/comments")
	public CommentResource getCommentResource() {
		return new CommentResource();
	} 
	
	private String gerUriForSelf(UriInfo uriInfo, Racer racer) {
		return uriInfo.getBaseUriBuilder()
				            .path(RacerResource.class)
				            .path(Long.toString(racer.getRacerId()))
				            .toString();
	}
	
	private String gerUriForComments(UriInfo uriInfo, Racer racer) {
		return uriInfo.getBaseUriBuilder()
				            .path(RacerResource.class)  // to obtain /racers
				            .path(RacerResource.class,"getCommentResource") // to obtain comment subresource, second parameter is method name of subresource
				            .path(CommentResource.class) // subresource /comments
				            .resolveTemplate("racerId", racer.getRacerId()) // to change {racerId} parameter with original racerId
				            .toString();
	}
	
}
