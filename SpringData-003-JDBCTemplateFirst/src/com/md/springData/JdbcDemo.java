package com.md.springData;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.md.springData.dao.JdbcDaoImpl;
import com.md.springData.model.Circle;

public class JdbcDemo {

	public static void main(String[] args) {
		
		@SuppressWarnings("resource")
		ApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
		JdbcDaoImpl dao = context.getBean("jdbcDaoImpl",JdbcDaoImpl.class);
		
		Circle circle = dao.getCircleById(1);
		System.out.println(circle.toString());
		circle = dao.getCircleById(3);
		System.out.println(circle.toString());
		
		System.out.println("Circle Count : " + dao.getCircleCount());
		System.out.println("Circle Name of 2: " + dao.getCircleName(2));
		
		
	}

}
