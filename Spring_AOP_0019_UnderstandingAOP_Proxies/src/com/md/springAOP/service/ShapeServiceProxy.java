package com.md.springAOP.service;

import com.md.springAOP.aspect.LoggingAspect;
import com.md.springAOP.model.Circle;

public class ShapeServiceProxy extends ShapeService {
	
	@Override
	public Circle getCircle() {
		
		//Before Aspect
		new LoggingAspect().BeforeLoggingAdvice();
		
		return super.getCircle();
		
	}

}
