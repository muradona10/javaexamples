package com.md.springAOP;

import com.md.springAOP.service.FactoryService;
import com.md.springAOP.service.ShapeService;

public class AopMain {

	public static void main(String[] args) {
		
		FactoryService factoryService = new FactoryService();
		ShapeService shapeService = (ShapeService) factoryService.getBean("shapeService");
		shapeService.getCircle();
	}

}
