package com.md.springAOP.aspect;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;


@Aspect
@Component
public class LoggingAspect {
	
	@Pointcut("within(com.md.springAOP.model.Circle)")
	public void allCircleMethods() {}
	
	@Before("allCircleMethods()")
	public void LoggingAdvice() {
		System.out.println("[Logging Advice]: Circle method run!");
	}
	
	/**
	 * @param radius
	 * @param newName
	 * Metoda ge�irdi�imiz isimler ile a�a��da kulland�klar�m�z ayn� olmal�...!
	 */
	@Before("args(radius, newName)")
	public void argumentsMethods(double radius,String newName) {
		System.out.println("[StringArgumentsMethods Advice], params:" + radius + " " + newName);
	}
	
	@After("args(radius, name)")
	public void afterAlways(double radius, String name) {
		System.out.println("This advice always runs after function, params: " + radius + " " + name);
	}
	
	@AfterReturning(pointcut="args(radius, name)", returning="returnObj")
	public void afterOnlyNoException(double radius, String name, Object returnObj) {
		System.out.println("This advice only runs after function completes normally");
		System.out.println("params: " + radius + " " + name);
		System.out.println("returns: " + returnObj);
	}
	
	@AfterThrowing(pointcut="args(radius, name)", throwing="ex")
	public void afterOnlyException(double radius, String name, Exception ex) {
		System.out.println("This advice only runs after function throws an exception");
		System.out.println("params: " + radius + " " + name);
		System.out.println("Exception: " + ex);
	}
	
}
