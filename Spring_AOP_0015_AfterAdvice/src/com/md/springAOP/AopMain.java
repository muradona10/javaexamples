package com.md.springAOP;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.md.springAOP.service.ShapeService;

public class AopMain {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		
		ApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
		ShapeService shapeService = context.getBean("shapeService", ShapeService.class);
		//ShapeService shapeService = (ShapeService) context.getBean("shapeService");
		shapeService.getCircle().setName("New Circle");
		System.out.println(shapeService.getCircle().getName());
		shapeService.getCircle().printCircle(10.4, "Last Circle");
		System.out.println("_____________________________________________________");
		shapeService.getCircle().printCircleWithException(2.5, "ExceptionCircle");
	}

}
