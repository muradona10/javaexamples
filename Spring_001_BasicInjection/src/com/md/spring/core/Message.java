package com.md.spring.core;

public class Message {
	
	private String channel;
	private int maxCharLength;
	
	public void sentMessage(String message) {
		if(message.length() <= this.getMaxCharLength()) {
			System.out.println(message + "\n\n" + "Channel: " + this.getChannel());
		}else {
			System.out.println("Maximum char is exceed!");
		}
	}
	
	public Message() {}
	
	public Message(String channel) {
		this.channel = channel;
	}
	
	public Message(int maxCharLength) {
		this.maxCharLength = maxCharLength;
	}
	
	public Message(String channel, int maxCharLength) {
		this.channel = channel;
		this.maxCharLength = maxCharLength;
	}

	public String getChannel() {
		return channel;
	}

	public int getMaxCharLength() {
		return maxCharLength;
	}

	
	
}
