package com.md.spring.core;

public class Mail {
	
	private String sign;

	public void sendMailWithSign(String message) {
		System.out.println(message + "\n\n" + getSign());
	}
	
	public Mail() {
		System.out.println("Default Constructor..!");
	}
	
	public Mail(String sign) {
		this.sign = sign;
	}
	
	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

}
