package com.md.spring.core;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		System.out.print("***************************************************************");
		System.out.print("SPRING SETTER INJECTION EXAMPLE");
		System.out.print("***************************************************************");
		System.out.println();
		/**
		 * This example displays how to setter injection in spring
		 * Read spring.xml file there is a bean there and its property is
		 * written clearly
		 */
		
		ApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
		Mail newMail = (Mail) context.getBean("mail");
		newMail.sendMailWithSign("Hello Spring..!");
		
		
		System.out.print("***************************************************************");
		System.out.print("SPRING CONSTRUCTOR INJECTION EXAMPLE");
		System.out.print("***************************************************************");
		System.out.println();
		
		/**
		 * This example displays how to constructor injection in spring
		 * in spring.xml file there is no property definition for Message class
		 * And Message class do not have any setter implementation for its member variables
		 */
		Message newMessage = (Message) context.getBean("message");
		newMessage.sentMessage("Hello Spring..!");
		System.out.print("***************************************************************");
		System.out.println();
		newMessage.sentMessage("Welcome to our Spring Core Examples");
		

	}

}
