package com.md.spring.core;

public class Triangle {
	
	private Point pointA;
	private Point pointB;
	private Point pointC;
	
	public void drawTriangle() {
		
		System.out.println("Poing x: (" + this.getPointA().getX() + "," + this.getPointA().getY() + ")");
		System.out.println("Poing y: (" + this.getPointB().getX() + "," + this.getPointB().getY() + ")");
		System.out.println("Poing z: (" + this.getPointC().getX() + "," + this.getPointC().getY() + ")");
		
	}

	public Point getPointA() {
		return pointA;
	}

	public void setPointA(Point pointA) {
		this.pointA = pointA;
	}

	public Point getPointB() {
		return pointB;
	}

	public void setPointB(Point pointB) {
		this.pointB = pointB;
	}

	public Point getPointC() {
		return pointC;
	}

	public void setPointC(Point pointC) {
		this.pointC = pointC;
	}
	
	

}
