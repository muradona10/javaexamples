package com.md.spring.core;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class DrawApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		ApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
		Triangle triange = (Triangle) context.getBean("triangle");
		triange.drawTriangle();
		
		/**
		 * spring.xml dosyas� i�erisinde Triangle objesinin tan�m� bulunuyor
		 * Buna g�re Triangle 3 tane Point tipinden nesne tutuyor, bu g�sterim ise
		 * Bean tan�mlama y�ntemlerinden propery tag inin ref �zelli�i sayesinde yap�l�yor
		 * E�er bir s�n�f yerine tip i�erseydi(string,int vs.) ref yerine value kullanacakt�k.
		 * Bir Triangle � olu�turmak i�in property �zelli�inde ref g�rd��� i�in spring �nce bu referanslardaki
		 * s�n�flar� olu�turmaya �al���yor. O referanslar da ba�ka referanslar i�erebilir en sona kadar gidip
		 * �nce onu olu�turarak ba�lar. 
		 */
		
	}

}
