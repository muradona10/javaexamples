package com.md.springData.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.md.springData.model.Circle;

/**
 * 
 * @author Murat Demir This class is responsible for the database connection and
 *         other database functions like query, insert, update etc.
 *
 */
public class JdbcDaoImpl {

	public Circle getCircleById(int circleId) {

		Connection conn = null;
		String driverName = "org.apache.derby.jdbc.ClientDriver"; // derby database connection string
		
		Circle circle = null;

		try {
			Class.forName(driverName).newInstance();
			conn = DriverManager.getConnection("jdbc:derby://localhost:1527/db"); // connect local database

			// query string 
			PreparedStatement ps = conn.prepareStatement("select * from circle where id = ?");
			ps.setInt(1, circleId);

			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				circle = new Circle(circleId, rs.getString("name"));
			}
			rs.close();
			ps.close();
		} catch (Exception e) {
			new RuntimeException(e);
		} finally {
			try {
				conn.close();
			}catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return circle;
	}

}
