package com.md.springData;

import com.md.springData.dao.JdbcDaoImpl;
import com.md.springData.model.Circle;

public class JdbcDemo {

	public static void main(String[] args) {
		
		Circle circle = new JdbcDaoImpl().getCircleById(1);
		System.out.println(circle.toString());
		circle = new JdbcDaoImpl().getCircleById(3);
		System.out.println(circle.toString());
		
		
	}

}
