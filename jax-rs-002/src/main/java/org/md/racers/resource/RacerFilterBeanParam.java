package org.md.racers.resource;

import javax.ws.rs.QueryParam;

public class RacerFilterBeanParam {
	
	private @QueryParam("teamName") String teamName;
    private @QueryParam("startIndex") int startIndex;
    private @QueryParam("size") int size;
    
	public String getTeamName() {
		return teamName;
	}
	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}
	public int getStartIndex() {
		return startIndex;
	}
	public void setStartIndex(int startIndex) {
		this.startIndex = startIndex;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
    
    
    

}
