package org.md.racers.resource;

import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

@Path("/params")
@Produces(MediaType.TEXT_PLAIN)
public class ParamResource {
	
	@GET
	public String getParams(@MatrixParam("matrixParam") String matrixParam,
			                @HeaderParam("authToken") String authToken,
			                @CookieParam("cookieName") String cookieName) {
		
		return "MatrixParam: " + matrixParam + "\n" + "HeaderParam: " + authToken + "\n" + "CookieParam: " + cookieName;
	}
	
	@GET
	@Path("/context")
	public String getContextInfo(@Context UriInfo uriInfo, @Context HttpHeaders httpHeaders) {
		String uriPath = uriInfo.getAbsolutePath().toString();
		String cookies = httpHeaders.getCookies().toString();
		return "path: " + uriPath + " cookie: " + cookies;
	}


}
