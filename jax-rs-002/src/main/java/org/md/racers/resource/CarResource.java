package org.md.racers.resource;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.md.racers.model.Car;
import org.md.racers.service.CarService;

@Path("/cars")
public class CarResource {
	
	private CarService carService = new CarService();
	
	@GET
	@Produces(MediaType.APPLICATION_XML)
	public List<Car> getCars(){
		return carService.getAllCars();
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_XML)
	public Car getCarsById(@PathParam("id") Long id) {
		return carService.getCarById(id);
	}

}
