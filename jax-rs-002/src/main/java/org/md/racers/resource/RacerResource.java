package org.md.racers.resource;

import java.util.List;

import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import org.md.racers.model.Racer;
import org.md.racers.service.RacerService;

@Path("/racers")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class RacerResource {
	
	RacerService racerService = new RacerService();
	
	@GET
	public List<Racer> getRacers(@BeanParam RacerFilterBeanParam racerFilterBeanParam){
		if(racerFilterBeanParam.getTeamName()!=null)
			return racerService.getRacersByTeam(racerFilterBeanParam.getTeamName());
		if(racerFilterBeanParam.getStartIndex()>0 && racerFilterBeanParam.getSize()>0)
			return racerService.getRacersPage(racerFilterBeanParam.getStartIndex(), racerFilterBeanParam.getSize());
		return racerService.getAllRacers();
	}
	
	@GET
	@Path("/{racerId}")
	public Racer getRacer(@PathParam("racerId") Integer id) {
		return racerService.getARacer(id);
	}
	
	@POST
	public Racer addRacer(Racer racer) {
		return racerService.addRacer(racer);
	}
	
	@PUT
	@Path("/{racerId}")
	public Racer updateRacer(Racer racer, @PathParam("racerId") Integer id) {
		return racerService.updateRacer(racer, id);
	}
	
	@DELETE
	@Path("/{racerId}")
	public boolean deleteRacer(Racer racer, @PathParam("racerId") Integer id) {
		return racerService.deleteRacer(racer, id);
	}
	
	@GET
	@Path("/context")
	public String getContextInfo(@Context UriInfo uriInfo, @Context HttpHeaders httpHeaders) {
		String uriPath = uriInfo.getAbsolutePath().toString();
		String cookies = httpHeaders.getCookies().toString();
		return "path: " + uriPath + " cookie: " + cookies;
	}

}
