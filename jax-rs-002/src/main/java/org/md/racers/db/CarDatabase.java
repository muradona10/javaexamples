package org.md.racers.db;

import java.util.HashMap;
import java.util.Map;

import org.md.racers.model.Car;

public class CarDatabase {
	
	private static Map<Long, Car> cars = new HashMap<>();
	
	public static Map<Long, Car> getCars(){
		return cars;
	}

}
