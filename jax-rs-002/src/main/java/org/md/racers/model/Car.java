package org.md.racers.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Car {
	
	private long carId;
	private String brand;
	private String madeInCountry;
	private long rate;
	
	public Car() {
	}
	
	public Car(long carId, String brand, String madeInCountry, long rate) {
		super();
		this.carId = carId;
		this.brand = brand;
		this.madeInCountry = madeInCountry;
		this.rate = rate;
	}
	public long getCarId() {
		return carId;
	}
	public void setCarId(long carId) {
		this.carId = carId;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getMadeInCountry() {
		return madeInCountry;
	}
	public void setMadeInCountry(String madeInCountry) {
		this.madeInCountry = madeInCountry;
	}
	public long getRate() {
		return rate;
	}
	public void setRate(long rate) {
		this.rate = rate;
	}
	
	

}
