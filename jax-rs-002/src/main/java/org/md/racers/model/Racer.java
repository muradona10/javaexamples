package org.md.racers.model;

import java.util.Date;

public class Racer {

	private Integer racerId;
	private String racerName;
	private String teamName;
	private Date racerBirthDate;
	
	public Racer() {
		
	}
	
	public Racer(Integer racerId, String racerName, String teamName, Date racerBirthDate) {
		super();
		this.racerId = racerId;
		this.racerName = racerName;
		this.teamName = teamName;
		this.racerBirthDate = racerBirthDate;
	}
	
	public Integer getRacerId() {
		return racerId;
	}
	public void setRacerId(Integer racerId) {
		this.racerId = racerId;
	}
	public String getRacerName() {
		return racerName;
	}
	public void setRacerName(String racerName) {
		this.racerName = racerName;
	}
	public String getTeamName() {
		return teamName;
	}
	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}
	public Date getRacerBirthDate() {
		return racerBirthDate;
	}
	public void setRacerBirthDate(Date racerBirthDate) {
		this.racerBirthDate = racerBirthDate;
	}
	
	
	
}
