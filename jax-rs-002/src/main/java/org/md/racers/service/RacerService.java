package org.md.racers.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.md.racers.model.Racer;

@SuppressWarnings("deprecation")
public class RacerService {
	
	public List<Racer> dummyRacers = Arrays.asList(
		new Racer(1, "Valentino Rossi", "Yamaha", new Date(1987, 1, 10)),
		new Racer(2, "Maverick Vinales", "Yamaha", new Date(1991, 10, 25)),
		new Racer(3, "Andrea Dovizioso", "Ducati", new Date(1985, 10, 12)),
		new Racer(4, "Marc Marquez", "Honda", new Date(1991, 6, 21)));
	
	public static List<Racer> racers = new ArrayList<>();
	
	public RacerService() {
		if(racers==null || racers.isEmpty())
		   racers.addAll(dummyRacers);
	}
	
	public List<Racer> getAllRacers(){
		return racers;
	}
	
	public List<Racer> getRacersByTeam(String teamName){
		return racers.stream().filter(r -> r.getTeamName().equals(teamName)).collect(Collectors.toList());
	}
	
	public List<Racer> getRacersPage(int startIndex, int size){
		if(startIndex+size<=racers.size())
		   return racers.subList(startIndex, startIndex+size);
		return null;
	}

	public Racer addRacer(Racer racer) {
		racers.add(racer);
		return racer;
	}
	
	public Racer getARacer(Integer id) {
		return racers.stream().filter(f->f.getRacerId().equals(id)).findFirst().get();
	}
	
	public Racer updateRacer(Racer racer, Integer id) {
		Racer r = getARacer(id);
		if(r!=null) {
			for(int i=0;i<racers.size();i++) {
				if(racers.get(i).getRacerId().equals(id)) {
					racers.remove(i);
					racers.add(i, racer);
					return racer;
				}
			}
		}
		return null;
	}
	
	public boolean deleteRacer(Racer racer, Integer id) {
		Racer r = getARacer(id);
		if(r!=null) {
			for(int i=0;i<racers.size();i++) {
				if(racers.get(i).getRacerId().equals(id)) {
					racers.remove(i);
					return true;
				}
			}
		}
		return false;
	}

}
