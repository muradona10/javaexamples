package org.md.racers.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.md.racers.db.CarDatabase;
import org.md.racers.model.Car;

public class CarService {
	
	private Map<Long, Car> cars = CarDatabase.getCars();
	
	public CarService() {
		cars.put(new Long(cars.size()+1), new Car(cars.size()+1, "BMW", "Germany", 5));
		cars.put(new Long(cars.size()+1), new Car(cars.size()+1, "Mercedes", "Germany", 5));
		cars.put(new Long(cars.size()+1), new Car(cars.size()+1, "Volvo", "Sweden", 5));
		cars.put(new Long(cars.size()+1), new Car(cars.size()+1, "Renault", "France", 3));
		cars.put(new Long(cars.size()+1), new Car(cars.size()+1, "Peugeot", "France", 3));
		cars.put(new Long(cars.size()+1), new Car(cars.size()+1, "Honda", "Japan", 3));
		cars.put(new Long(cars.size()+1), new Car(cars.size()+1, "Ferrari", "Italy", 5));
		cars.put(new Long(cars.size()+1), new Car(cars.size()+1, "Volkswagen", "Germany", 4));
		cars.put(new Long(cars.size()+1), new Car(cars.size()+1, "AUDI", "Germany", 5));
		cars.put(new Long(cars.size()+1), new Car(cars.size()+1, "Ford", "America", 4));
	}
	
	
	public List<Car> getAllCars(){
		return  new ArrayList<>(cars.values());
	}
	
	public Car getCarById(Long id) {
		return cars.get(id);
	}
	

	public Map<Long, Car> getCars() {
		return cars;
	}

	public void setCars(Map<Long, Car> cars) {
		this.cars = cars;
	}
	
	

}
