package org.md.advancedJaxRS.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.md.advancedJaxRS.model.Book;

public class BookService {
	
	private List<Book> books = new ArrayList<Book>();
	
	public BookService() {
		if(books==null || books.size()==0) {
			books.add(new Book(1, "Ejderha Dovmeli Kiz", "Stieg Larson"));
			books.add(new Book(2, "Bulbulu Oldurmek", "Harper Lee"));
			books.add(new Book(3, "Kurk Mantolu Madonna", "Sabahattin Ali"));
			books.add(new Book(4, "Fatih Harbiye", "Peyami Safa"));
		}
	}

	public List<Book> getBooks() {
		return books;
	}
	
	public void setBooks(List<Book> books) {
		this.books = books;
	}

	public Book getBookById(Integer bookId) {
		return books.stream().filter(b->b.getId().equals(bookId)).findFirst().get();
	}

	public Book addBook(Book newBook) {
		System.out.println("new book is adding...");
		books.add(newBook);
		System.out.println(newBook);
		return newBook;
	}

	public List<Book> getBookByAuthor(String author) {
		List<Book> list = books.stream()
				    .filter(b->b.getAuthor().equalsIgnoreCase(author))
				    .collect(Collectors.toList());
		return list;
	}
	
}
