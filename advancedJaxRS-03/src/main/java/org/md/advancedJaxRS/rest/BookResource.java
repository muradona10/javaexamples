package org.md.advancedJaxRS.rest;

import java.net.URI;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.md.advancedJaxRS.model.Book;
import org.md.advancedJaxRS.service.BookService;

@Path("/books")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class BookResource {
	
	private BookService bookService = new BookService();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Book> getBooks(@QueryParam("author") String author) {
		if(author!=null && !author.isEmpty())
			return bookService.getBookByAuthor(author);
		return bookService.getBooks();
	}
	
	@GET
	@Path("/{bookId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Book getBookWithId(@PathParam("bookId") Integer bookId) {
		return bookService.getBookById(bookId);
	}
	
	@POST
	public Response addBook(Book newBook, @Context UriInfo uriInfo) {
		System.out.println("adding new book...");
		bookService.addBook(newBook);
		String newId = String.valueOf(newBook.getId());
		URI uri = uriInfo.getAbsolutePathBuilder().path(newId).build();
		return Response.created(uri)
				       .entity(newBook)
				       .build();
	}

}
