package org.md.advancedJaxRS.client;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.md.advancedJaxRS.model.Book;

public class RestAPIClient {

	public static void main(String[] args) {
		
		Client client = ClientBuilder.newClient();
		Response response = client.target("http://localhost:8080/advancedJaxRS-01/webapi/books/1").request().get();
		Book book = response.readEntity(Book.class);
		System.out.println(book.toString());
		
		Book book2 = client.target("http://localhost:8080/advancedJaxRS-01/webapi/books/2")
			      .request(MediaType.APPLICATION_JSON)
			      .get(Book.class);
		System.out.println(book2.toString());
		
		String book3 = client.target("http://localhost:8080/advancedJaxRS-01/webapi/books/3")
		      .request(MediaType.APPLICATION_JSON)
		      .get(String.class);
		System.out.println(book3.toString());
	}

}
