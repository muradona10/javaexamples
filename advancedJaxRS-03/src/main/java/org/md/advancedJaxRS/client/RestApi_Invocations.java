package org.md.advancedJaxRS.client;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.md.advancedJaxRS.model.Book;

public class RestApi_Invocations {

	public static void main(String[] args) {
		
		RestApi_Invocations demo = new RestApi_Invocations();
		Invocation booksByAuthor = demo.getBooksByAuthor("Peyami Safa");
		Response response = booksByAuthor.invoke();
		System.out.println(response.getStatus());    
		System.out.println(response.readEntity(Book.class));

	}
	
	public Invocation getBooksByAuthor(String author) {
		
		Client client = ClientBuilder.newClient();
		
		return client.target("http://localhost:8080/advancedJaxRS-01/webapi/")
				                     .path("books")
				                     .queryParam("author", author)
				                     .request(MediaType.APPLICATION_JSON)
				                     .buildGet();  // build request and get Invocation.
		
	}

}
