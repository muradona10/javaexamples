package org.md.advancedJaxRS.client;

import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.md.advancedJaxRS.model.Book;

public class RestApi_GenericType {

	public static void main(String[] args) {
		
		RestApi_GenericType demo = new RestApi_GenericType();
		List<Book> books = demo.getBooksByAuthor("Peyami Safa");
		books.forEach(System.out::println);
		System.out.println("--------------------------------------------------------------");
		books = demo.getAllBooks();
		books.forEach(System.out::println);
	}
	
	public List<Book> getBooksByAuthor(String author) {
		Client client = ClientBuilder.newClient();
		
		return client.target("http://localhost:8080/advancedJaxRS-01/webapi/")
				                     .path("books")
				                     .queryParam("author", author)
				                     .request(MediaType.APPLICATION_JSON)
				                     .get(new GenericType<List<Book>>() {});
	}
	
	public List<Book> getAllBooks() {
		Client client = ClientBuilder.newClient();
		
		return client.target("http://localhost:8080/advancedJaxRS-01/webapi/")
				                     .path("books")
				                     .request(MediaType.APPLICATION_JSON)
				                     .get(new GenericType<List<Book>>() {});
	}

}
