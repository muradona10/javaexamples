package org.md.advancedJaxRS.client;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.md.advancedJaxRS.model.Book;

public class RestAPIClient2 {

	public static void main(String[] args) {
		
		Client client = ClientBuilder.newClient();
		
		WebTarget baseTarget = client.target("http://localhost:8080/advancedJaxRS-01/webapi/");
		WebTarget booksTarget = baseTarget.path("books");
		WebTarget simpleBookTarget = booksTarget.path("{bookId}");
		
		String book4 = simpleBookTarget.resolveTemplate("bookId", "4")
			                           .request(MediaType.APPLICATION_JSON)
			                           .get(String.class);
		System.out.println(book4.toString());
		
		/**
		 * Add Post Request
		 */
		Book newBook = new Book(5, "Olasiliksiz", "Adam Fawer");
		Response response = booksTarget.request().post(Entity.json(newBook));
		
		if(response.getStatus() != 201) {
			System.out.println("Post Error!");
			System.out.println(response);
		}else {
			Book createdBook = response.readEntity(Book.class);
			System.out.println(createdBook);
		}
		

	}

}
