package com.md.springAOP.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import com.md.springAOP.model.Circle;

@Aspect
@Component
public class LoggingAspect {
	
	@Pointcut("within(com.md.springAOP.model.Circle)")
	public void allCircleMethods() {}
	
	@Before("allCircleMethods()")
	public void LoggingAdvice() {
		System.out.println("[Logging Advice]: Circle method run!");
	}

	@Before("allCircleMethods()")
	public void LoggingAdviceWithJoinPoint(JoinPoint joinPoint) {
		System.out.println("[LoggingAdviceWithJoinPoint Advice]");
		System.out.println("[Short String]:" + joinPoint.toShortString());
		System.out.println("[Long String] :" + joinPoint.toLongString());
		System.out.println("[To String]   :" + joinPoint.toString());
		System.out.println("[Kind]        :" + joinPoint.getKind());
		
		for(Object o : joinPoint.getArgs()) {
			System.out.println("param:" + o.toString());
		}
		
		Circle circle = (Circle) joinPoint.getTarget();
		System.out.println(" [Target]     :" + circle.toString());
		
	}
	
	/**
	 * @param radius
	 * @param newName
	 * Metoda ge�irdi�imiz isimler ile a�a��da kulland�klar�m�z ayn� olmal�...!
	 */
	@Before("args(radius, newName)")
	public void stringArgumentsMethods(double radius,String newName) {
		System.out.println("[StringArgumentsMethods Advice], params:" + radius + " " + newName);
	}

}
