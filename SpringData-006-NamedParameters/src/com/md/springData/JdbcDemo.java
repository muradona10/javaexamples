package com.md.springData;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.md.springData.dao.JdbcDaoImpl;
import com.md.springData.model.Circle;

public class JdbcDemo {

	public static void main(String[] args) {
		
		@SuppressWarnings("resource")
		ApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
		JdbcDaoImpl dao = context.getBean("jdbcDaoImpl",JdbcDaoImpl.class);
		
		dao.clearCircleTable();
		System.out.println("----------------------------------------------");
		dao.insertCircle(new Circle(1, "Circle 1"));
		dao.insertCircle(new Circle(2, "Circle 2"));
		dao.insertCircle(new Circle(3, "Circle 3"));
		List<Circle> circles = dao.getAllCircles();
		circles.stream().forEach(c->System.out.println(c.getName()));
		System.out.println("----------------------------------------------");
		dao.insertCircleWithNamedParameter(new Circle(4, "Circle 4"));
		dao.insertCircleWithNamedParameter(new Circle(5, "Circle 5"));
		dao.insertCircleWithNamedParameter(new Circle(6, "Circle 6"));
		circles = dao.getAllCircles();
		circles.stream().forEach(c->System.out.println(c.getName()));
	}

}
