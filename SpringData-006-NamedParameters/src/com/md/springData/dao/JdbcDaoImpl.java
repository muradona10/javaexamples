package com.md.springData.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import com.md.springData.model.Circle;

/**
 * 
 * @author Murat Demir This class is responsible for the database connection and
 *         other database functions like query, insert, update etc.
 *
 */
@Component
public class JdbcDaoImpl {
	
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplate;
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	public int getCircleCount() {
		String sql = "select count(*) from circle";
		int circleCount = jdbcTemplate.queryForObject(sql, Integer.class);
		return circleCount;
	}
	
	public String getCircleName(int circleId) {
		String sql = "select name from circle where id = ?";
		return jdbcTemplate.queryForObject(sql, new Object[] {circleId}, String.class);
	}
	
	public Circle getCircleById(int circleId) {
		String sql = "select * from circle where id = ?";
		List<Circle> circles = jdbcTemplate.query(sql, new Object[] {circleId}, new CircleMapper());
		if(circles!=null && circles.size()>0)
		   return circles.get(0);
		else return null;
	}
	
	public List<Circle> getAllCircles() {
		String sql = "select * from circle";
		return jdbcTemplate.query(sql, new CircleMapper());
	}
	
	public void insertCircle(Circle circle) {
		Circle c = getCircleById(circle.getId());
		if(c == null) {
			String sql = "insert into circle(id, name) values(?, ?)";
			jdbcTemplate.update(sql, new Object[] {circle.getId(), circle.getName()});
		}else {
			System.out.println("This circle is already exist in db!");
		}
	}
	
	public void insertCircleWithNamedParameter(Circle circle) {
		Circle c = getCircleById(circle.getId());
		if(c == null) {
			String sql = "insert into circle(id, name) values(:id, :name)";
			SqlParameterSource paramSource = new MapSqlParameterSource("id",circle.getId())
					                                         .addValue("name", circle.getName());
			namedParameterJdbcTemplate.update(sql, paramSource);
		}else {
			System.out.println("This circle is already exist in db!");
		}
	}
	
	public void clearCircleTable() {
		String sql = "delete from circle where 1=1";
		jdbcTemplate.update(sql);
	}
	
	private static final class CircleMapper implements RowMapper<Circle> {

		@Override
		public Circle mapRow(ResultSet resultSet, int rowNum) throws SQLException {
			
			int id = resultSet.getInt("ID");
			String name = resultSet.getString("NAME");
			Circle circle = new Circle(id,name);
			return circle;
		}

	}
	
	public DataSource getDataSource() {
		return dataSource;
	}

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
		return namedParameterJdbcTemplate;
	}

	public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}
	
	

}
